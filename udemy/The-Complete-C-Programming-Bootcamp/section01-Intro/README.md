# Section 1 

## Overview

* [1-Starting-with-c.md](./1-Starting-with-c.md)
* [2-Printing-to-The-Console.md](./2-Printing-to-The-Console.md)
* [3-Variables-Console-Input.md](./3-Variable-Console-Input.md)
* [4-Convert-To-Hex.md](./4-Convert-To-Hex.md)
* [example](./example)
  * [example1.c](./example/example1.c)
  * [example2.c](./example/example2.c)


