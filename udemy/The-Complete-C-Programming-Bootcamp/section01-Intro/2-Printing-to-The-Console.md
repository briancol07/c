# Printing to the Console 

## To compile the file in linux 

> execute the next command and will compile in c11 

```bash
gcc -std=c11 program.c -o program 

```

## Printf

``` c 
printf("hello world \n");
```

printf is a function, and a string is pass through parameter 

see [example1.c](./example/example1.c)


\n represent a enter (a new line) 
