# Starting with C


## Strengths 

* High speed and efficiency 
* The forerunner of the most modern technologies 
* Performance critical systems 
* Close contact with bare metal
* Widely popular: 1 place in TIOBE index

## As A Foundation Technology 

* Kernels (linux)
* System programming 
* Databases (Redis) 
* Git 
* RTOs (Real time OS)
* Iot 

## Programming Flow in C 

* Source File (.c)
  * Design 
  * Edit 
  * Compile
* Object File (.obj)
  * Link
* Executable File (.exe)
  * Run 

