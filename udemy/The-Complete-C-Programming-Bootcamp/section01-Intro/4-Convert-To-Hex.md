# Convert to Hexadecimal

To get an alignment we can put this way 

``` c 
int a = 123; 

pritnf("value %9d",a);
```

This will give spaces until get the space of 9 between value and the variable a 


% | description 
--|---------
d | Decimal 
x | Hexadecimal 


``` c
printf("value Hex: %9x \n",a);
```

see example2 [example2.c](./example/example.c)
