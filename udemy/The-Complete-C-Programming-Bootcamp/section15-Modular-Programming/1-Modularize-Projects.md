# Modularize your Projects

## Thinking in Modules 

* When you write large programs, organize the funtionalities into modules 
* Think of the source file as a module, not a single function or even a bunch of code
* Top-Down approach: from big task, divide to small problems
* Bottom-up Approach: by aggregating simple features you get a big program.


```
┌──────────────────────────────┐
│Main                          │
└──────────────────────────────┘
┌──────────────┐┌──────────────┐
│Module 1      ││Module 2      │
│Task A | TaskB││Task C | TaskD│ 
└──────────────┘└──────────────┘
```

## How to modularize

* Information Hiding 
  * Think of a module as a black box 
  * From the outisde you're only interested in interfacing with the module
* Low Coupling, High cohesion 
  * task that work on the same data should be in a single module 
  * Task related to the same functionality should be in the same module 
  * Minimize coupling between different modules 
* Design for change 
  * You should foresee future maintenance and evolution of the system 
  * Don't break the whole system when tweaking just one task

## Sharing Modules 

* The interface to a module is defined in the header file (module\_name.h)
* The actual implementation of a module is in the source file (module\_name.c)
* When you want to use another module you have to include its header file 
* Inclusion is done with the #include preprocessor directive 

``` c 
// The preprocessor will look for these headers files in compiler or system predefined folders.

#include<stdlib.h>
#include<strings.h>
```
``` c
// The preprocessor will look for these header files in the same folder as your program or in an explicitly specified path

#include "my_module.h"
#include "include/topic/thing.h>
```

## Using Modules

* stack.h contains the prototypes of the functions defined in stack.c
* stack.h must also be included in stack.c
* All source files that want to use the functionabilities exposed by stack.h must include it 
* stack.h hides the actual implementation of stack.c 

``` c
// in stack.h
void push (int);
int pop();
```

``` c 
// in stack.c 

#include "stack.h"

void push(int val {/* code */}
int pop() { /* code }
```

``` c
// in main.c
# include "stack.h"

int main(){
  int some_val = 123;
  push(some_val);
  int ret_val = pop();
}
```

```
┌────┐    ┌────────────┐
│code│--> │Preprocessor│
└────┘    └────────────┘
              |
              V
          ┌────────┐
          │Compiler│
          └────────┘
              |
              V 
┌────┐    ┌──────┐
│Exec│<-- │Linker│
└────┘    └──────┘
```

if you have multiple files refering to the same header may cause problems , so we must use include guards 

``` c 
#ifndef LED_H
#define LED_H

#endif 
```

