# Integers 

## int vs Unsigned int 

int = positives and negatives
unsigned int = positives 

unsigned int can be called unsigned 

## Examples 

``` c 

int temperatureFahrenheit = -10;

// The U can be use to say that is unsigned 

unsigned int temperatureKelvin = 250U;

```

## Bytes used in memory

Type | Size (bytes) 
-----|-------------
short int | usually 2 
int | at least 2, usually 4 
long int | at least 4, usually 8
long long int | at least 8 

<br>
The same with unsigned 

## stdint 

```c 
#include <stdint.h>
``` 

Type | Description 
-----|-------------
int8\_t | Signed int 8 bit wide 
int16\_t | Signed int 16 bit wide  
int32\_t | Signed int 32 bit wide  
int64\_t | Signed int 64 bit wide  
uint8\_t | Unsigned int 8 bit wide 
uint16\_t | Unsigned int 16 bit wide 
uint32\_t | Unsigned int 32 bit wide 
uint64\_t | Unsigned int 64 bit wide 

### Limits 

* Minimum values of exact-width signed integer types:
  * INTN_MIN = $` -2^{n-1} `$
* Maximum values of exact-width signed integer types:
  * INTN_MAX = $` 2^{n-1} -1 `$
* Maximum values of exact-width unsigned integer types:
  * UINTN_MAX = $` 2^n - 1 `$
* Unsigned minimum is zero 

## Format specifiers 

Type | Format specifier 
-----|-----------------
short int | %hd 
int | %d 
long int | %ld 
long long int | %lld 
unsigned short int | %hu 
unsigned int | %u 
unsigned long int | %lu
unsigned long long int | %llu

``` c 
// long literal 
long int hugeNumber = 1234567890000000L; 
```

see [example1.c](./example/example1.c)

