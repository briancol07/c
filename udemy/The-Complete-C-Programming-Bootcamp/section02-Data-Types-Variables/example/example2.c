#include<stdio.h>
#include<stdlib.h>


int main () {
  double decimalDigits = 1.23456789;
  double worldPopulation = 7.504E9;
  double plankConstnat = 6.626E-34 ;

  // 1 digit 
  printf("%12.1f | 1 decimal digit \n",decimalDigits);
  // 3 digit 
  printf("%12.3f | 3 decimal digit \n",decimalDigits);

  
  printf("%12.1f | worldPopulation  \n",worldPopulation);

  printf("%12.1e | worldPopulation  \n",worldPopulation);
  printf("%12.3e | worldPopulation  \n",worldPopulation);

  
  printf("%12.1E | plankConstnat  \n",plankConstnat);
  printf("%12.1E | plankConstnat  \n",plankConstnat);

  return EXIT_SUCCESS;
}
