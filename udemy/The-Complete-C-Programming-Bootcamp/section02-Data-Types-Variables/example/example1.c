#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

int main(){
  unsigned int student = 24U;
  unsigned long long int worldPopulation = 780123456945U;

  printf("%12u | Students in the class \n",student);
  
  printf("%12llu | world Population \n",worldPopulation);
  
  uint8_t count = UINT8_MAX;
  printf("%12u | Count of something \n",count);

  count ++;
  printf("%12u | count of something +1 (overflow)  \n ",count);

  printf("%12d | INT32_MIN \n ",INT32_MIN);
  printf("%12d | INT32_MAX \n ",INT32_MAX);
  
 return EXIT_SUCCESS; 

}
