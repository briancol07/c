# Characters 

```c 

char c = 'a';

```

* 1 byte -> 8 bit signed int 
* Range: -128 -> +127
* ASCII characters 


## Escape sequences 


type |value | Description 
-----|------|-------------
\\b  | 0x08 | Backspace 
\\n  | 0x0A | Newline 
\\r  | 0x0D | Carriage Return 
\\t  | 0x09 | Horizontal tab 
\\v  | 0x0B | Vertical tab 
\\\  | 0x5C | Backslash
\\'  | 0x27 | Single quotation mark 
\\"  | 0x22 | Double quotation mark 


