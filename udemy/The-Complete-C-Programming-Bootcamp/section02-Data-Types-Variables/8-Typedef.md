# Typedef 

```c

typedef existing_name alias_name;
typedef unsigned char byte;

byte b = 0xFF;

```

Explicit names for new data types  

see [example4.c](./example/example4.c)



