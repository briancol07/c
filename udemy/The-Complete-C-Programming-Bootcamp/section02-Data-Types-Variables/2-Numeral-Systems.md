# Numeral Systems 

How computer see the numbers 

## Integers 

* No fractional component 
* Positive, negative, Zero
* Discrete values 

* Base 10: Decimal  
  * { 0,1,2,3,4,5,6,7,8,9 }
  * Positional notation 
* Base 2 : Binary 
  * Only 2 : { 0,1 }
  * Positional notation 
* Base 8 
* Base 16 


## Overflow 

* 8 bits 
* Minimum values: 
  * 0b00000000 = 0 
* Maximum value: 
  * 0b11111111 = 255
* Sum 1 to the max and will return to 0
* range 
  * 0 -> $` 2^N -1 `$

## Two's Complement 

To represent negative numbers 

* 8 bit signed 
* maximum value: 
  * 0b0111111 = 127 
* split in 8 , left most significant bit is 0, if 0 positive 1 negative 
* range $` -2^{n-1} -> 2^{n-1} -1 `$

## Real number 

* Positive, Negative, Zero
* Continuous values 
* Decimal representation 

## Scientific notation

* Conveninet with too big or to small numbers
* Used by scientists, mathematicians and engineers 
* $`  0.1234 = 1234 x 10^{-4} `$ 
* Significand(or mantiza m ) x base(b) ^ exponent(e)
* Normalized Scientific Notation: 1 <= |m| < 10 

## Floating Point 

> IEEE 754 

name|common-name|significand-bits|Decimal-digits|Exponent-bits|Decimal-max
----|-----------|----------------|--------------|-------------|-----------
binary32 | Single precision | 24 | 7.2 | 8 | 38.2
binary64 | Double precision | 53 | 15.9| 11 | 307.9
