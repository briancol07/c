# Data Types and Variables 

## Overview 

* [1-Variables-and-Constants.md](./1-Variables-and-Constants.md)
* [2-Numeral-Systems.md](./2-Numeral-Systems.md)
* [3-Integers.md](./3-Integers.md)
* [4-Floating-Point.md](./4-Floating-Point.md)
* [5-characters.md](./5-characters.md)
* [6-booleans.md](./6-Booleans.md)
* [7-Enum.md](./7-Enum.md)
* [8-Typedef.md](./8-Typedef.md)
* [challenges] (./challenges)
  * [challenge1.c](./challenges/challenge1.c)
  * [challenge2.c](./challenges/challenge2.c)
* [example](./example)
  * [example1.c](./example/example1.c)
  * [example2.c](./example/example2.c)
  * [example3.c](./example/example3.c)
  * [example4.c](./example/example4.c)

