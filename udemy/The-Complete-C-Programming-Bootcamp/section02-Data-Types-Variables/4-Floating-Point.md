# Floating point


```c 
// 32-bit floating point 
float pi = 3.14;

// 64-bit floating point 

double pi = 3.14;

// Scientific Notation literal

double pi = 314E-2;

```


* More digits of precision 
* Small numbers
* Bigger numbers 

## Format Specifiers 

* Decimal Floating point
  * %f 
* Scientific Notation 
  * %e  


## Example 

* %\[width]\[.precision]specifier
* see [example2](./example/example2.c)


