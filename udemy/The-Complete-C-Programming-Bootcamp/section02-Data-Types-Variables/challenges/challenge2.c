#include<stdio.h>
#include<stdlib.h>

/*******************************************************************************
 * Define the following numeric constants:
 * - moonLanding: 1969
 * - speedOfLight : 299792458
 * - pi : 3.142
 * - hexaDead : 0xDEAD
 * - hexaSecret: 51966 
 * 
 * Print the following formats: 
 * - MoonLanding :
 *   - Integer, 10 characters width, padded with spaces 
 *   - Integer, 10 characters width, padded with zeroes 
 * - sppedOfLight
 *   - floating point without digits after decimal point 
 *   - scientific notation with 3digit of precision 
 * - pi:
 *   - Floating point with 2 digit of precision
 *   - scientific notation with 1 digit of precision. sign forced 
 * - HexaDEAD:
 *   - unsigned hexadecimal integer, uppercase, leading 0x 
 *   - unsigned integer, 6 character width 
 * - HexaSecret
 *   - Unsigned hexadecimal integer, lowercase, no leading 0x 
 *
 * 
*******************************************************************************/

int main(){
  const int moonLanding = 1969;
  const double speedOfLight = 299792458;
  const double pi = 3.142;
  const unsigned hexaDead = 0xDEADU;
  const unsigned hexaSecret = 51966U;

  printf("Moon landing: \n %10d \n %010d \n ",moonLanding,moonLanding);
  printf("Speed of light \n %.0f \n %.3e \n",speedOfLight,speedOfLight);
  printf("Pi \n %.2f \n %+.1e \n",pi,pi);
  printf("Hexa dead \n 0x%X \n %6u \n",hexaDead,hexaDead);
 printf("Hexa secret: \n %x \n",hexaSecret); 



  

}
