#include <stdio.h>
#include <stdlib.h>


/************************************************************************
 *
 * We are working on the man-machine interface of a revolutionary spacship.
 * Define enumeration constnats for different predifined thrust levels, these
 * levels will be used to comunicate with the engine controller 
 *
 * The thrust levels are:
 * - none : 0 
 * - low : 5
 * - medium : 9 
 * - high : 12 
 * - maximum : 20 
 *
 *  Then launch the rocket! 
 *  We now want to print the thrust level in some critical points:
 *  -- set thrust to non --> print ready to go level 
 *  -- set thrust to maximum --> print take off level 
 *  -- set thrust to medium --> print entering into the ionosphere
 *  -- set thrust to low --> print traveling to deep space 
 *
 *************************************************************************/


typedef enum {
  THRUST_NONE,
  THRUST_LOW = 5,
  THRUST_MEDIUM = 9,
  THRUST_HIGH = 12,
  THRUST_MAXIMUM = 20, 
}SpaceshipThurst ;


int main() {


  thrust = THRUST_NONE
  printf("Ready to go level %d\n",thrust);
  thrust = THRUST_MAXIMUM
  printf("Take off level %d\n",thrust);
  thrust = THRUST_MEDIUM
  printf("Entering into the ionosphere %d\n",thrust);
  thrust = THRUST_LOW
  printf("Traveling to deep space %d\n",thrust);

  return 0 ;
}
