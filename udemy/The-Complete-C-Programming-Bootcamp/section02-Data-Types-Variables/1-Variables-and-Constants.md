# Variables and Constants 


## Data manipulation 

8 bits -> 1 byte 
4 bytes -> 1 word 

1 word have an address in memory 

Variable is a containter. 

``` c 

// example 
// data-type Identifier  = Literal  

int girls = 12 ; 
const int boys = 12; 

```


> Strongly types -> Variables types cannot be changed 

## Rules 

* Only letters (upper and lower case), digits(0-9) and underscore (\_) 
* The first character can't be a digit
* Avoid leading underscore 

## use a consistent naming style 

* snake case : my_fancy_variable, total_width, next_value
* Camel case : myFancyVariable, totalWidth, nextValue 

choose meaningful names 

> Any fool can write code that a computer can understand. Good programmers write code that humans can understand. --Martin Fowler 

> Alwats code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live --John Woods  

## What is a data Type? 

It's a bundle of compile-time properties for an object 

* Memory size and alignment 
* Set of valid Values/Range 
* Set of permitted operations 

## Data Types (general) 

* Numbers 
* Characters 
* Strings 
* Array 
* Complex types 
