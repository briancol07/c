# Booleans 

```c 
// before c99

int a = 1; // True 
int b = 0; // False 

// with library 

#include <stdbool.h>

bool a = true;
bool b = false;

```


