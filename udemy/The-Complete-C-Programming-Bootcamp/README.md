# The Complete C Programming Bootcamp 

> c is the key

## Overview 

* [Intro](./section01-Intro/README.md)
* [Data Types Variables](./section02-Data-Types-Variables/README.md)
* [Operators](./section03-Operators/README.md)
* [Control FLow](./section04-Control-Flow/README.md)
* [Functions](./section05-Functions/README.md)
* [Debugging](./section06-Debugging/README.md)
* [Pointers](./section07-Pointers/README.md)
* [Arrays](./section08-Arrays/README.md)
* [Strings](./section09-Strings/README.md)
* [Dynamic Memory Allocation](./section10-Dynamic-Memory-Allocation/README.md)
* [Structures](./section11-Structures/README.md)
* [Unions](./section12-Unions/README.md)
* [Preprocesor Macros](./section13-Preprocesor-Macros/README.md)
* [File Input Output](./section14-File-Input-Output/README.md)
* [Modular Programming](./section15-Modular-Programming/README.md)


