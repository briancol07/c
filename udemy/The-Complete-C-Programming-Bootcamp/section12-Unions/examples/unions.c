#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

typedef struct {
    uint8_t enable : 1 ; 
    uint8_t ready : 1;
    uint8_t mode : 2 ;
    uint8_t something_else : 2;
}RegBits;

/*
    Union 
        - Allow to store different data types in the same memory allocation 
        - each member of a union shares the same memory location
*/

typedef union{
    uint32_t word;
    struct {
        uint8_t byte1;
        uint8_t byte2;
        uint8_t byte3;
        uint8_t byte4;
    }bytes;
} HardwareRegisterA;


typedef union{
    RegBits bits; // mapping various bits in a single one 
    uint8_t byte;
} HardwareRegisterB;

void print_bits(void *data, int size){
    uint8_t *bytes = data;
    for(int i = 0; i < size;i++){
        for (int j = 0; j < 8 ; j++){
            printf("%d",(bytes[i] >> j)&1); // shift the register and show 1 
        }
    }
    printf("\n");
}



int main(){

    HardwareRegisterA regA = {.byte = 0x01};

    printf("regA: 0x%02X, ",regA.byte);
    print_bits(&regA, sizeof(regA));

    regA.bits.mode = 3;

    printf("regA: 0x%02X, ",regA.byte);
    print_bits(&regA, sizeof(regA));
    

    HardwareRegisterB regB = {.word = 0XAABBCCDD};

    printf("regB: %X, \n",regB.word);

    regB.bytes.byte2 = 0 ;
    printf("regB: %X, \n",regB.word);
    return EXIT_SUCCESS;
}