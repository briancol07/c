# Preprocesor and Macros 

## Topics 

* Preprocessor role
* Hash directives 
* Macros Vs Functions 
* Pitfalls to avoid 

## Overview

* [1-Preprocessor-Directives.md](./1-Preprocessor-Directives.md)
* [examples](./examples/examples)
  * [1-Simple-Macros.c](./examples/1-Simple-Macros.c)
  * [2-Function-like-Macros.c](./examples/2-Function-like-Macros.c)
  * [3-Conditional-Compilation.c](./examples/3-Conditional-Compilation.c)

