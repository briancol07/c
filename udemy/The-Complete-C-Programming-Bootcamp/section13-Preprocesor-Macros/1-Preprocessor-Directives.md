# Preprocessor Directives 

## C Preprocessor 

* Not part of the compiler
* Separate step in the compilation Process 
* Perform simple text replacements 
* Pre-Process the source files before the actual compilation
* Preprocessor commands are called directives
* All these directives begin with a hash symbol (#)

┌─────────────┐   ┌────────────┐   ┌────────┐
│C Source Code│-->│Preprocessor│-->│Compiler│
└─────────────┘   └────────────┘   └────────┘

## Macros 

* Can be used to optimized code 

## Preprocessor Directives


Type    | Description   
--------|-------------
\#define | Substitutes a preprocessor macro
\#include | Inserts a praticular header from another file
\#undef | Undefines a preprocessor macro
\#ifdef | Returns true if this macro is defined
\#ifndef | Returns true if this macro is not defined
\#if | Tests if a compile time condition is true 
\#else | The alternative to the previous \#if 
\#elif | Abbreviation of \#else and \#if in one statement
\#endif | Ends preprocessor conditional
\#error | Prints error message on stderr
\#pragma | Issues special commands to the compiler 

## Predefined Macros 

Type |Description    
-----|------------
\_\_DATE\_\_ | The current date as a string literal in "MMM DD YYYY" format 
\_\_TIME\_\_ | The current time as a string literal in "HH:MM:SS" format
\_\_FILE\_\_ | The current filename as a string literal
\_\_LINE\_\_ | The current line number as an integer
\_\_STDC\_\_ | The value is 1 when the compiler compiles with the ANSI standard
