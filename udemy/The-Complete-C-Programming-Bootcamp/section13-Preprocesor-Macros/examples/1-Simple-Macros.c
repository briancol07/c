#include<stdio.h>
#include<stdlib.h>

#define ARRAY_LEN 100

int main(){

  double array[ARRAY_LEN];

  printf("Array length %d\n", ARRAY_LEN);

// when the preprocessor occur this will not appear in our code 
// Try to put them always to the left (no indentation)
#undef ARRAY_LEN 
#define ARRAY_LEN 999

  printf("Array length %d\n", ARRAY_LEN);

  printf("Source file \"%s\", %d \n",__FILE__,__LINE__);
  printf("Compilation time %s\n",__TIME__);

  return EXIT_SUCCESS;
}
