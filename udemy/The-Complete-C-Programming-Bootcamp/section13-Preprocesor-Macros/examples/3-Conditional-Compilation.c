#include<stdio.h>
#include<stdlib.h>

#define LOG_INFO
#define BUFFER_SIZE 1024

int main(){


// This is not consider by the compiler 
//
#ifdef SOME_MACRO
  printf("SOME_MACRO\n");
#endif

#ifdef LOG_INFO
  printf("This is an Info\n");
#else
  printf("I shouldn't log anything: \n");
#endif 

  // if the buffer don't exist will enter the else 
  // we need to do a check only if the buffer exist 

//#if BUFFER_SIZE > 2048
 // printf("The buffer is huge");
//#else
 // printf("The buffer is ok \n");
//#endif

  //Correct way 
#if defined BUFFER_SIZE && BUFFER_SIZE > 2048
  printf("The buffer is huge");
#elif defined BUFFER_SIZE
  printf("The buffer is ok\n");
#else
  printf("You forgot to define BUFFER_SIZE \n");
#endif
  

  return EXIT_SUCCESS;
}
