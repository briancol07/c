#include<stdio.h>
#include<stdlib.h>

#define ARRAY_LEN 100 

//int calc_array_len(double array[]){ this we cant use it we lose sizeof 

// name(parameters) (things to do)
#define CALC_ARRAY_LEN(x) (sizeof(x) / sizeof(x[0]))

#define MAX(a,b) (a > b ? a:b)
// good practice 
// A correct way is to write in parenthesis #define MAX(a,b) ((a) >(b) ? (a):(b))
// Also in the firts one CALC_ARRAY_LEN(x) (sizeof((x)) / sizeof((x)[0]))


int main(){

  double array[ARRAY_LEN];

  printf("Array length %d\n",ARRAY_LEN);

#undef ARRAY_LEN 
#define ARRAY_LEN 999

  printf("Array length %d\n",ARRAY_LEN);

  int array_len = sizeof(array) / sizeof(array[0]);
  int array_Macro = CALC_ARRAY_LEN(array);

  printf("normal %d\n", array_len);
  printf("Function macro %d \n",array_Macro);

  int a = -1 , b = 5;

  printf("MAX(%d,%d): %d\n",a,b,MAX(a,b));

  int b_before = b;
  printf("value of b : %d",b);
  // this is a problem because the macro is extended in ( a > b++ ? a :b++)
  // So b will be seven 
  printf("MAX(%d,%d): %d\n",a,b,MAX(a,b++));
  printf("value of b : %d",b);

  return 0;
}
