#include<stdio.h>
#include<stdlib.h>
#include<string.h>


int main(){

  char my_string[] = "I love programming in c!";
  printf("The string \" %s \" \n", my_string);
  
  int i;
  for (i = 0; my_string[i] != '\0'; i++);
  printf("string length (from scratch : %d \n",i);
  printf("Sizeof string %lld \n", sizeof(my_string));
  printf("String length (built in): %lld \n", strlen(my_string));

  char another_string = " Programming is funny";

  char another_long_string [] = "This is a dummy very very long string ...";

  printf("Before copy \" %s \" \n",my_string);
  printf("After copy \" %s \" \n",strncpy(my_string, "hello", 3)); // The null char is missing 
  printf("Before copy \" %s \" \n",my_string);
  printf("After copy \" %s \" \n",strncpy(my_string, "hello", sizeof(my_string)));
  printf("Before copy \" %s \" \n",my_string);
  strncpy(my_string, another_long_string,sizeof(my_string));
  my_string[sizeof(my_string)-1] = '\0';
  printf("After copy: \" %s\" \n ", my_string);

  return 0;
}
