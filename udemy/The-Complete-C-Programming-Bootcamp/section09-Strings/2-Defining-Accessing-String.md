# Defining  and accessign a String

## Declaring a String 

``` c
char fancy_string [10];
```

* A string is nothing more than an array of characters 
* So, there are no built-in operators in C for processing strings
* The standard library provides many functions for strings 

## Initializing a String 

``` c
char str[] = {'H','e','l','l','o','!','\0'};

char str[10] ={'H','e','l','l','o','!' };

char str2[] = "Hello";

```
## Accessing a String 

```c 
// 1 
char str[] = "hello";

// 2 
str [0] = 'Y';

// 3 ERROR 
str = "Another string"; 
```

## Constant String 

```c 
// 1 
const char str[] = "hello!";

// str [0] = 'Y' --> ERROR 

// 2 
char *str = "HELLO";
// str[0] = 'Y'; --> ERROR  (runtime)
str = "Another string"; // this is allowed because is a pointer  

```

## Display String 

```c 
char str[] = "This course ";
printf("%s \n",str);
printf("%s \n", &str[4]); // Start printing from the 4 to end 
printf("%s.4s \n",str);   // This wil print until the 4 char 
```
## Array of strings 

``` c 
char days[][10] = {
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
};

for(int i = 0 ; i < 7 ; i++){
  pritnf(%s\n", days[i]);
}
// We can do pointers 

char *days[] = {
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
};

```

## Acquire String 

``` c 
char input_str[10];
scanf("%s",input_str);
```

* Define the length of **input_str** carefully! 
* Ignore whitespace characters before a non-whitespace character 
* Then, will stop at the first whitespace character found 
* Read just one word!

