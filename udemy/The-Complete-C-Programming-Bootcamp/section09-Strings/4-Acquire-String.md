# Acquire String 


``` c 
char input_str[10];
scanf("%s", input_str);
```

* Define the length of input\_str carefully!
* Ignore whitespace characters before a non-whitespace character 
* Then, will stop at the first witespace character found 
* Read just one word! 
