#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define BUF_SIZE 5

/*
*   Implement a function that acquires a line from the console and has this prototype
*   int get_line(char dst[], int size);
*
*   Goals:
*     - Don't use scanf 
*     - Acquire a string up to the newline char 
*     - Always put anull character at te end of the buffer 
*     - Truncate the acquisition to leave space at the end of the buffer for the null character
*     - Retrun the number of characters acquires, excluding the null character 
*     - Implement a main loop that ask the user to insert a line, aquire it and print it 
*     - Terminate the main loop when the user enter END 
*/

int get_line(char dst[], int size);
void clear_stdin();
int main(){
  char buffer[BUF_SIZE];
  do{
    printf("Entera line \n");
    int n = get_line(buffer,BUF_SIZE);
    printf("[%d] %s \n", n, buffer);

  }while(strcmp(buffer, "END"));

  return EXIT_SUCCESS;
}

int get_line(char dst[], int size){
  int i ;
  for (i = 0; i < size -1 ; i++){
    int c = getchar();
    if (c == '\n' || c == EOF){
      break;
    }else {
      dst[i] = (char) c;
    }
  }
  if (i == size-1){
    clear_stdin();
  }
  dst[i] = '\0';
  return i;
}

void clear_stdin(){
  int c = ' ';
  while(c != '\n' && c == EOF ) {
    c = getchar(); 
  }
}
