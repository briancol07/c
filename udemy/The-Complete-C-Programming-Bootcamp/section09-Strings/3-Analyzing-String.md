#
Analyzing and Converting String 

## Analyzing String 

```c
#include<ctype.h>
```

* Sometimes you need to test or map the characters of a string 
* You can use ctype from the standard library 

## Characters Classes 


 Class | Description    
-------|-------------
 Digit | 0 1 2 3 4 5 6 7 8 9
 Hexadecimal digits | 0 1 2 3 4 5 6 7 8 9 A B C D E F a b c d e f 
 Lowercase Letters | a b c d e f g h i j k l m n o p q r s t u v w x y z 
 Uppercase Letters | A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 
 Punctuation Characters | ! " # $ % ^ & * () + - , . / ; : < = > ? @ [] \ _ ' \` {} | ? ~
 Alphabetic characters | a Set of lowercase letters and Uppercase letters
 Alphanumeric characters | a Set of digits, lowercase letters and uppercase letters 
 Graphical characters | a Setof alphanumeric characters and punctuation characters 
 Space characters | a Set of tab, newline, vertical tab, form feed, carriage return, and space
 Printable characters | a Set of alphanumeric characters, Punctuation characters and space characters
 Control characters | Between ASCII codes 0x00 (NULL) and 0x1f (US), plus 0x7f (del) 

## Testing Characters 

Function | Description 
---------|-------------
int islower(int c) | checks if c is a lowercase letter 
int isupper(int c) | checks if c is an uppercase letter
int isalpha(int c) | checks if c is alphabetic 
int isalnum(int c) | checks if c is alphanumeric 
int isdigit(int c) | checks if c is a decimal digit
int isxdigit(int c)| checks if c is a hexadecimal digit
int iscntrl(int c) | checks if c is a control character 
int isprint(int c) | checks if c is a printable character 
int isgraph(int c) | checks if c is a printable excluding spaces 
int isspace(int c) | checks if c is white-space
int ispunct(int c) | checks if c is a punctuation character 

### Parameter (int c)

This is the character to be checked. It's an integer, representable as an usigned char

### Return value (int)

These functions return non-zero (true) if the argument satisfies the condition, or zero (false) if not.

## Converting Letters

Function | Description 
---------|------------
int tolower(int c) | Converts uppercase letters to lowercase
int toupper(int c) | Converts lowercase letters to uppercase

### Parameter (int c)

This is the character to be converted. It's an integer, representable as an unsigned char 

### Return value (int)

These functions return lowercase or uppercase conversion, if such value exists, else c remains unchanged 

## Converting Strings to Numbers 

``` c
#include<stdlib.h>
```

* Functions to convert strings to integers or floating points
* Beware that some functions have poor error handling 

Functions | Description 
----------|--------------
int atoi (const char \*str) | Converts str to an integer (int)
long int atol (const char \*str) | Converts str to a long integer (long int)
double atof (const char \*str) | Convert str to a double
double strtod (const char \*str, char \*\*endptr) | Convert str to a double
long int strtol (const char \*str,char \*\*endptr, int bas) | Converts str to a long integer (long int) 

* The first three :
  * If the value cannot be represented, the behavior is undefined ! 
* The last two: 
  * If the value cannot be represented, a range error occurs and errno is set to ERANGE 

