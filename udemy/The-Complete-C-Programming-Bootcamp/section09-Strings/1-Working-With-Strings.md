# Working with Strings 

## What is a string ? 

> An ordered collection of characters 

Secuence of ordered blocks 



Address| Data| Hex | Char| Index  
-------|-----|-----|-----|-------
0x1004 | 01101111| 0x6F | 'o'| 4
0x1003 | 01101100| 0x6C | 'l'| 3
0x1002 | 01101100| 0x6C | 'l'| 2
0x1001 | 01100101| 0x65 | 'e'| 1
0x1000 | 01101000| 0x68 | 'h'| 0


```c 
// Character literal
char c = 'A';

// String literal
printf("Hello world \n");
``` 

## Escape sequences 

Type| hex | Description 
----|-----|-----------
\n| 0x0A| Newline (line feed)
\r | 0x0D| Carriage Return 
\b | 0x08| Backspace 
\t | 0x09| Horizontal Tab
\v | 0x0B| Vertical tab
\\\ | 0x5C| Backslash
 \' | 0x27| Single quotation mark 
 \" | 0x22| Double quotation mark 
 \0 | 0x00 | Null character 

## NULL character 

* Special character used to mark the end of a string 
* Do not confuse with **NULL** ! 
  * NULL -> Address, is guaranteed not point to any memory location 
  * \0   -> Character, string terminator 

``` 
                0123456789101112
"Hello World!"  Hello Worl d !\0

```
