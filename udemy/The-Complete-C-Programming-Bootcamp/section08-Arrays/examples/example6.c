#include<stdio.h>
#include<stdlib.h>

#define Len 5
#define N_ROWS 3 
#define N_COLS 4 
#define N_LAYERS 5

int main(){

  // int array [10] -> 1 dimension 
  
  // Matrix -> bi dimensional array 
  // Three or more is tensor
  
  int matrix [N_ROWS][N_COLS] = { // n = N_ROWS * N_COLS

    {11,12,13,14},
    {21,22,23,24},
    
  }; 
  int tensor [N_LAYERS][N_ROWS][N_COLS]= { // n =N_LAYERS * N_ROWS * N_COLS
    {
      {111,112,113,114}, // N_COLS
      {121,122,123,124}, 
    },//N_ROWS
    {
      {211,212,213,214},
      {221,222,223,224},
    }


  };//N_LAYERS 


  for (int i = 0; i < N_ROWS;i++){
    for(int j = 0 ; j < N_COLS; j ++){
      printf("%2d",matrix[i][j]);
    }
    printf("\n");
  }
  
  for (int i = 0; i < N_LAYERS;i++){
    for(int j = 0 ; j < N_ROWS; j ++){
      for(int k = 0 ; k < N_COLS; k++){
        printf("%2d",tensor[i][j][k]);
      }
      printf("\n");
    }
    printf("\n");
  }


  return 0;
}
