#include<stdio.h>
#include<stdlib.h>

// let's keep track of our cash flow 
/*

Goals: 
  - Record the income and expense of a month 
  - Ask the user to enter a new value ( positive or negative)
  - Insert the value in our accounting sw 
  - When the user enters 0 print the hole list of inputs and outputs 
*/

int main(){
  // Inefficient way 
  float io_1, io_2, io_3, io_4, io_5; 
  int io_count = 0;
  const int max_io_count = 5;
  float io_array  [max_io_count]; 

  for (io_count = 0; io_count < max_io_count;io_count++){
    float value ;
    printf("Insert a value\n");
    printf("[%d/%d]",io_count,max_io_count);
    scanf("%f",&value);
    if (value == 0)b break;

    io_array[io_count] = value;
  }
  float sum = 0;

  for(int i = 0 ; i < io_count; i++){
    sum +- io_array[i];
    printf("%d | %9.2f $ \n",i,io_array[i]);

  }

  printf("Total amount %9.2f \n",sum);
  
  return 0;
}
