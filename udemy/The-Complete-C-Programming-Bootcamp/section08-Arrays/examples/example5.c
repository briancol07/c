#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

#define LEN 10 

int sum_arr(int arr[], const int len){

//  printf("%lld \n",sizeof(arr)); this is an error like *arr[]
  int sum = 0; 
  for (int i = 0; i < len ; i++){
    sum += arr[i];
  }
  return sum;

}
int sum_ptr(int * const p, const int len){

  int sum = 0; 
  for (int i = 0; i < len ; i++){
    sum += p[i];
  }
  return sum;

}

int sum_ptr_arithmetic(const int *const p , const int len){
  int sum = 0;
  for (const int *p_item = p ; p_item < p + len; p_item ++){
    sum += *p_item;
    printf("%lld , %d\n",(int64_t)p_item,sum;
    printf("the offset %lld ",p_item - p); 
  }
  return sum;
}


int main(){

  // Pointers arithmetics 

  int x [LEN] = {1,4,-3,2};

  printf("sum_arr: %d \n", sum_arr(x,LEN)); 
  printf("sum_arr: %d \n", sum_ptr(x,LEN)); 

  int *ptr = x ; // same as : int *ptr = &x[0] 
  printf("%lld, %d \n",(uint64_t)ptr ,*ptr); // address value 
  ptr++; 
  printf("%lld, %d \n",(uint64_t)ptr ,*ptr); // go to the next position of the array 

  printf("-----------\n");

  pritnf(" sum_ptr_arithmetic: %d \n"sum_ptr_arithmetic(x,LEN);

  /* Illegal operations 
   *
   *  Pointer + Pointer 
   *  Pointer / pointer 
   *  pointer * pointer 
   *  pointer % pointer 
   *
  */

  return 0;
}


