#include<stdio.h>
#include<stdlib.h>

// # for the preprocesor ( a macro) 
#define len 5
// we cannot use variable or constants with brace-initialization



int main(){

//  const int len = 5;
  int a[len]; // array type int[5] being declared 

  // before moving we need to contain all 1s 

  for (int i = 0; i< len ; i++){
    a[i] = 1;
  }
  // array initialization from brace-enclosed lists

  int x[] = {1,2,3,4,5};// x has a type int[5] and holds 1,2,3,4,5

// this is an error int y[len] = {1,2,3}; if we difine a const int of 5 
  int y[len] = {1,2,3}; // this work and the other two index are completed with 0 
  int z[len] = {0}; // this contains a bunch of zeros 

  int w[len] = {[1] = 1, [3] = 3};
  int q[len] = {[1] = 3 , [len -3] =5};

  int t[len] = {[len - 3] =4,3,2}; // the next element are secuencial , this array contains 0 0 3 2 1 
  return 0;
}
