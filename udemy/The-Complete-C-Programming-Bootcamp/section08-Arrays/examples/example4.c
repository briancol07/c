#include<stdio.h> 
#include<stdlib.h>

int main(){

  int x[] = {9,8,7,6,5};

  for (int i = 0; i < sizeof(x) / sizeof(x[0]);i++){
    printf("x[i]: %d\n",x[i]);
  }

  printf("Access an element of x through pointer\n");
  int *p = &x[2];
  printf("x[2]: %d , *p : %d \n",x[2],*p);

//  int *alias = x ;
//  int *alias = &x[0];
  
  p = x; 
  printf("Display x thorugh the pointer \n");

  for (int i = 0; i < sizeof(x) / sizeof(x[0]);i++){
    printf("x[i]: %d, p[i]:%d \n",x[i],p[i]);
  }

  return 0;
}
