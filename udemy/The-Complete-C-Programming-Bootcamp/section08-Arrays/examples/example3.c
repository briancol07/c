#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

int main(){
  int x[] = {1,2,3,4,5};

  printf("Sizeof(x) %llu \n",sizeof(x));
  printf("Sizeof(x[0]) %llu \n",sizeof(x[0]));
  printf("Sizeof(int) %llu \n",sizeof(int));
  printf("array Lenght %llu \n", sizeof(x) / sizeof(x[0]));
  
  int8_t y[] = {1,2,3,4,5};

  printf("Sizeof(y) %llu \n",sizeof(y));
  printf("Sizeof(y[0]) %llu \n",sizeof(y[0]));
  printf("Sizeof(int8_t) %llu \n",sizeof(int8_t));
  printf("array Lenght %llu \n", sizeof(y) / sizeof(y[0]));
  return 0;
}
