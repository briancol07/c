# Arrays 

## Topics 

* Understand arrays 
* Arrays and pointers
* Pointer arithmetic 
* Multidimensional 
* Variable length 

## Overview 

* [1-Working-The-Wrong-way.md](./1-Working-The-Wrong-way.md)
* [challenges](./challenges)
  * [challenge1.c](./challenges/challenge1.c)
  * [challenge2.c](./challenges/challenge2.c)
* [examples](./examples)
  * [example1.c](./examples/example1.c)
  * [example2.c](./examples/example2.c)
  * [example3.c](./examples/example3.c)
  * [example4.c](./examples/example4.c)
  * [example5.c](./examples/example5.c)
  * [example6.c](./examples/example6.c)
  * [example7.c](./examples/example7.c)
