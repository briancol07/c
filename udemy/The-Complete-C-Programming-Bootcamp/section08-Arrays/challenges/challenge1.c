// Challenge 1 

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<time.h>

#define MIN_ROWS 2
#define MAX_ROWS 10 
#define MIN_COLS 2
#define MAX_COLS 10 
#define MIN_UPPER_LIMIT 2 
#define MAX_UPPER_LIMIT 1000 


/*
  Generate a random matrix and display it in direct transposed form 
  
  Goals: 
    - Generate random matrix with number of rows and columns given by the user 
    - Check that 1 < rows <= 10 , the same for the columns 
    - Generate random matrix with 0 <= number < upper limit given by the user 
    - Check that 1 < upperlimit < 1000
    - Display the matrix in direct and transposed form 
  
  Tips:
    - To generate random number below an upper bound you may dinf the modulo operator useful


*/

void generate_matrix (const int rows , const int cols, int matrix [rows] [cols], int upper_limit){

  for(int i = 0 ; i < rows ; i++){
    for( int j = 0 ; j < cols ; j++){
      matrix[i][j] = rand() % upper_limit;
    }
  }
}

void display_matrix(const int rows, const int cols , int matrix[rows][cols], bool transposed ){

if (! transposed){
  for(int i = 0 ; i < rows ; i++){
    for( int j = 0 ; j < cols ; j++){
      printf("%4d", matrix[i][j]);
    }
    printf("\n");
  }
}else { 
  for (int i = 0 ; i < cols ; i++){
    for (int j = 0; j <rows ; j++){
      printf("%4d", matrix[j][i];
    }
    printf("\n");
  }
 }
}

void display_matrix1(const int rows, const int cols , int matrix[rows][cols], bool transposed ){
  for (int i = 0; i < ( transposed ? cols : rows) ; i++){
    for (int j = 0 ; j < (transposed ? rows : cols);j++){
      printf("%4d",matrix[transposed ? j : i ] [transposed i :j] );
    }
  }
}


int main(){
  int rows, cols , upper_limit;

  printf("Enter the number of rows (min %d, max %d \n",MIN_ROWS,MAX_ROWS);
  scanf("%d",&rows);

  if (rows < MIN_ROWS || rows > MAX_ROWS){
    printf("out of bounds \n");
    return EXIT_FAILURE;
  }
  printf("Enter the number of cols (min %d, max %d \n",MIN_COLS,MAX_COLS);
  scanf("%d",&cols);

  if (cols < MIN_ROWS || cols > MAX_ROWS){
    printf("out of bounds \n");
    return EXIT_FAILURE;
  }
  int matrix[rows][cols];
  srand(time(NULL));
  generate_matrix(rows, cols, matrix, upper_limit);
  display_matrix (rows, cols, matrix, false);
  display_matrix (rows, cols, matrix, true);
  
  return 0;
}
