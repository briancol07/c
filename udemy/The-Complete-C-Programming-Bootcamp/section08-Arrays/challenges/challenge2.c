// Challenge 2 prime numbersfuther improved 

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include<math.h>
#include<time.h>

#define MIN_UPPER_LIMIT 3
#define MAX_UPPER_LIMIT 1000000
#define PRIME_FOUND_LEN 100000
#define PRINTABLE_LIMIT 50 

/*
  Goals:
    - Find all prime number below a given upper limit given by the user 
    - Check that 2 < upper_limit <= 1 million
    - Store the prime number in an arrat of 100k integer 
    - Optimize the program by usign stroed prime numbers
    - Compare the execution time of the simple version with the optimized one 
    - Implement printing of primes found separately from the algorithm 
    - Print up to 50 prime number found 

  Simple version:
    _ Given an input number n , check whether it is evenly divisible by ANY NUMBER 
    between 2 and sqrt(n) 

  Optimized version:
    - Given an input number n , check whether it is evenly divisible by ANY PRIME NUMBER 
    between 2 and sqrt(n) 
*/

bool is_prime_simple(int num){
  const int limit = sqrt(num);
  for (int i = 2 ; i <= limit ; i++){
    if (num % i == 0){
      // num is venly divisible by i --> not prime 
      return false; 
    }
  }
  return true;
}

bool is_prime_optimized( int num, int prime_numbers[], int found){
  const int limit = sqrt(num);
  for (int i =0; i <found && prime_numbers[i] <= limit ; i++){
    if (num %prime_number[i] ==0){
      return false;
    }
  }
  return true;
}

bool is_prime(int num, bool optimized, int prime_numbers[], int found){
  return optimized ? is_prime_optimized(num, prime_numbers, found) : is_prime_simple(num);
  
}

int get_prime_number( int upper_limit, int pirme_number[], bool optimized){
  int found = 0;
  for (int num = 2 ; num < upper_limit; num++){
    if (is_prime(num, optimized, prime_number, found)){
      prime_number[found] = num;
      found ++;
    }
  }
  return found;
}

void print_number(int numbers[], int len){
  printf("Found %d prime numbers, \n", len);
  for (int i = 0 ; i < len && i < PRINTABLE_LIMIT; i++){
    printf("%d", numbers[i]); 
  }
  printf("\n");

}

double elapsed_time_sec(clock_t start , clock_t end){
  return (end - start) / (double) CLOCKS_PER_SEC;
}

int main(){
  int upper_limit;
  
  printf("Enter an upper limit :\n");
  scanf ("%d",&upper_limit);

  if (upper_limit < MIN_UPPER_LIMIT || upper_limit > MAX_UPPER_LIMIT){
    printf("out of bound\n");
    return EXIT_FAILURE;
  }
  int found, prime_number[PRIME_FOUND_LEN];
  clock_t start_clk , end_clk;

  start_clk = clock(); // start counter ticks elapsed since the program was launched 
  found = get_prime_number(upper_limit, prime_numbers, false);
  end_clk = clock(); 
  printf("\n simple veriosn elapsed %.1f [ms] \n", elapsed_time_sec(start_clk, end_clk) * 1e3);
  print_number(prime_nubmers, found);


  start_clk = clock(); // start counter ticks elapsed since the program was launched 
  found = get_prime_number(upper_limit, prime_numbers, true);
  end_clk = clock(); 
  printf("\n optimized veriosn elapsed %.1f [ms] \n", elapsed_time_sec(start_clk, end_clk) * 1e3);
  print_number(prime_nubmers, found);

  return 0;
}
