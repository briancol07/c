# Working with arrays the wrong way 

> Array is an ordered collection of items of the same type 

see [example1.c](./examples/example1.c)

sequential data blocks in memory, all of the same type, and all of these data items share an index which increases starting from the first up to the last one 

32 bit integer 

Address|              Data                 |Value|Index| 
------|------------------------------------|----|---
0x1010|00000000 00000000 00000000 00010000 | 16 | 4
0x100C|00000000 00000000 00000000 00001000 | 8  | 3
0x1008|00000000 00000000 00000000 00000100 | 4  | 2
0x1004|00000000 00000000 00000000 00000010 | 2  | 1
0x1000|00000000 00000000 00000000 00000001 | 1  | 0

```c
//array of 5 elements 

int a[5];
int b =5 ;
int b_array[b];

```

to access an array we can use a for loop
