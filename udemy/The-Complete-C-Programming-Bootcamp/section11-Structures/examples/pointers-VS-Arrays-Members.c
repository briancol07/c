
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>


#define MAX_DIMENSIONS 3

typedef struct GuyP{
  char *name;
  char *job;
  bool active;
  int dimensions;
  double pos[MAX_DIMENSIONS];

} GuyP;

typedef struct GuyA{
  char *name;
  char job[64];
  bool active;
  int dimensions;
  double pos[MAX_DIMENSIONS];

} GuyA;

int main(){
  printf("Size of (guyP): %lld\n", sizeof(GuyP));
  
  printf("Size of (guyA): %lld\n", sizeof(GuyA));

  GuyP billP = {
    .name = "Bill gates",
    .job = " I make software",
    .active = true,
  };

  bill.job = " I founded microsoft";

  GuyA billA = {
    .name = "Bill gAtes",
    .job = " I make software",
    .active = true,
  };
 // billA.job = " I founded microsoft" //this will give an error (left value) no assign a full string to an array with = 

  strncpy(billA.job , "I founded microsoft", sizeof(billA.job) -1 ); 
  
  return 0;
}

