#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

typedef struct LedStatus{

  uint8_t led_1;
  uint8_t led_2;
  uint8_t led_3;

}LedStatus;

typedef struct LedStatusBitFields{

  uint8_t led_1 : 1; // bit  
  uint8_t led_2 : 1;   
  uint8_t led_3 : 1;   

}LedStatusBitFields;

void print_bits(uint8_t *data, int size){
  for (int i = 0 ; i < size ; i++){
    uint8_t byte = data[i];
    for (int j = 0 ; j < 8 ; j++){
      printf("%d",(byte >> j) & i);
    }
    
    }
}

int main(){
  printf("sizeof(LedStatus): %lld\n",sizeof(LedStatus));
  printf("sizeof(LedStatus): %lld\n",sizeof(LedStatusBitFields));

  LedStatusBitFields status = {
    .led_1 = 1;
    .led_2 = 1;
    .led_3 = 1;
    };

  status.led_1=0;
  status.led_2=0;

  print_bits((uint8_t*) &status, sizeof(status));

  return EXIT_SUCCESS;


}
