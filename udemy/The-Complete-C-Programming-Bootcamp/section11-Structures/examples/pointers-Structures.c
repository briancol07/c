#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define MAX_DIMENSIONS 3

typedef struct Guy{
  char *name;
  bool active; 
  int dimensions;
  double pos[MAX_DIMENSIONS];
  struct Guy *friend;
}Guy;

int main(){
  Guy bill = {
    .name = "bill Gates",
    .pos = {3,2,1},
    .dimensions = 3,
    .active = true,
  };

  Guy *pointerToGuy = &bill;

  // 1) accessing the struct directly 

  printf("Guy's name %s \n", bill.name);

  // 2) accessing the struct thorugh pointers using the indirection operator 

  printf("Guy's name %s \n", (*pointerToGuy).name);

  // 3) Accessing the struct thorugh pointers using the arrow operator 

  printf("Guy's name %s \n", pointerToGuy->name); 

  // waling a single linked list 

  pointerToGuy->friend = malloc(sizeof(Guy));
  pointerToGuy->friend->name = "Elon musk";
  pointerToGuy->friend->active = false;

  // Allocating an array of guys 

  int num =10 ; 
  Guy *manyGuys = malloc(num * sizeof(Guy));

  manyGuys[0].name = "mario";
  manyGuys[0].friend = &bill;

  // accessing the 2nd element of manyGuys using pointers 

  (manyGuys + 1)->name = "Tom";
  printf("ManyGuys[1].name = %s \n",manyGuys[1].name);

  return 0; 
  

}
