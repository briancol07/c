#include<stdio.h>
#include<stdlib.h>
#include<math.h>


// Struct Declaration 

struct Guy{
  char *name;
  bool active;
  int dimensions;
  // cannot use incomplete array but...
  double pos[]; // flexible array member 

};

/* allasing the type with typedef
 * 
 * Annonimous structure 
 *
 * typedef struct{ 
 *  }Guy;
 *
 *  allasing the type with typedef (good practice)
 *  to refer a Guy friend in the anonimous is not available 
 *
 *  typedef struct Guy{
 *  struct Guy *friend 
 *  } Guy;
 */

double calc_distance(int dimensions, double a[dimensions], double b[dimensions]){
  double square_distance = 0;
  for (int i = 0 ; i < dimensions; i++){
    square_distance += pow(a[i]-b[i],2);

  }
  return sqrt(square_distance);
}


int main(){

  struct Guy elon, bill; 

  /* Implicit initialization
   * 
   * Guy elon = {
   *  "Elon musk",
   *  false,
   *  3,
   *  {1,2,3}
   * };
   * 
   * Explicit initialization
   *
   * Guy bill{
   *  .name = "Bill Gates",
   *  .pos = { 1,3,2},
   *  .dimension = 3,
   *  .active = true,
   * };
   *
   *
   * */

  elon.name = "Elon Musk";
  elon.active = true;
  elon.dimensions = 3;
  elon.pos[0] = 1;
  elon.pos[1] = 2;
  elon.pos[2] = 3;

  bill.name = "Elon Musk";
  bill.active = true;
  bill.dimensions = 3;
  bill.pos[0] = 1;
  bill.pos[1] = 2;
  bill.pos[2] = 3;

  if(!elon.active || !bill.active){
    printf("%s isn't active now \n",!elon.active ? elon.name : bill.name);
    return EXIT_FAILURE;
  }
  if(elon.dimensions != bill.dimensions){
    printf("They are in different dimensions\n");
    return EXIT_FAILURE;
  }
  printf("The distance between %s and %s is: %.1f\n",elon.name,bill.name,calc_distance(elon.dimensions,elon.pos,bill.pos);


  return EXIT_SUCCESS; 
}
