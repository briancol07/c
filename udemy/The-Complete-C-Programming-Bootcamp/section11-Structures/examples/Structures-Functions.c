
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>

#define MAX_DIMENSIONS 3 

typedef struct Guy{
  char *name;
  bool active;
  int dimensions;
  double pos[MAX_DIMENSIONS];
  struct Guy *friend;
}Guy;

Guy create_bill(){
  return (Guy) {
    .name = "bill gates",
    .pos = {1,2,3},
    .dimensions = 3,
    .active = true,
  };
}

Guy *alloc_elon(){
  Guy *elon = malloc(sizeof(Guy));
  if (!elon){
    return NULL;
  }
  elon->name = "Elon musk";
  elon->active = true;
  elon->dimensions = 3 
//  elon->pos[0] = 1;
  double pos [] = {1,2,3};
  memecpy(elon->pos,pos,sizeof(pos);
  return elon ;
}

void desactivate_guy(Guy g){
   g.activate = false;
}
void desactivate_guy_ptr(Guy *pg){
   pg->activate = false;
}


int main(){
  Guy bill = create_bill();
  bill.friend = alloc_elon();
  desactivate_guy(bill);
  descativate_guy_ptr(&bill);
  
  return EXIT_SUCCESS;
}
