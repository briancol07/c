# Structures 

## Topics 

* Use Structures 
* Structure Arrays
* Nested structures 
* Structures and Pointers
* Structures and Functions 

## Overview

* [examples](./examples)
  * [bit-Field.c](./examples/bit-Field.c)
  * [example1.c](./examples/example1.c)
  * [example2.c](./examples/example2.c)
  * [pointers-Structures.c](./examples/pointers-Structures.c)
  * [pointers-VS-Arrays-Members.c](./examples/pointers-VS-Arrays-Members.c)
  * [Structures-Functions.c](./examples/Structures-Functions.c)
* [challenges](./challenges)
  * [Heart-Rate-Monitor.c](./challenges/Heart-Rate-Monitor.c)
