
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

// Build a heart rate monitor

/*
  heart rte formula  = hr_max = 208  - 0.7 * age
  zones 
  1 | below 50 % | rest 
  2 | 50 - 60 | very light, active recovery 
  3 | 60 - 70 | light, cardio training improves your general endurance 
  4 | 70 - 80 | moderate, aerobic fitness, improve your efficiency 
  5 | 80 - 90 | hard, raise the anaerobic threshold, improve your speed endurance 
  6 | above 90 | peak performance , improve your power , only for pros! 

 - Goals ask for the user age 
 - print hte hr-max
 - print each training zone in the format "id, hrmin , hr max , description" 
 - in loop ask for the currnt BPM 
 - print the current zone in the format id, description
*/

typedef struct {
  int id;
  double low; // lower threshold
  char *str; // description 
  
}Zone;

double calc_hr_max(int age){
  return 208 - 0.7 * age;
}

void pirnt_row(int id, double lower, double upper , char str[]){
  printf("%d | %3.0f - %3.0f | %s \n",id, lower,upper,str);
}

void print_table(Zone zones[], int len, double hr_max){
  for (Zone *z = zones; z < zones + len ; z++){
  double upper =  z M zones + len -1 ? (z+1)-> low * hr_max :0 ; 
  print_row(z->id, z->low * hr_max , upper, z->str);
    
  }
}

Zone *find_zone(Zone zones, int zones_len, double hr_now, double hr_max){
  for(int i = zones_len -1 ;i > = 0; i--){
    if (hr_now > zones[i].low* hr_max){
      return &zones[i];
    }

  }
  return NULL ;
}

void print_zone(Zone *zone){
  pritnf("Zone %d: %s \n\n", zone->id, zone->str);
}

int main(){

  Zone zones []{
    {.id = 1 , .low = 0, .str = "REST"},
    {.id = 2 , .low = 0.5, .str="Very light, active recovery"},
    {.id = 3 , .low = 0.6, .str="light, cardio training, improves your general endurance"},
    {.id = 4 , .low = 0.7, .str="moderate aerobic fitness, improve your efficiency"},
    {.id = 5 , .low = 0.8, .str="Hard, raise the anaerobic htreshold improve your speed endurance"},
    {.id = 6 , .low = 0.9, .str="Peak performance, imrpove your power, only for pros!"},
  }

  int zones_len  = sizeof(zones) / sizeof(zone[0]);
  int age; 
  printf("enter your age");
  scanf("%d",&age);

  double hr_max = calc_hr_max(age);
  print_table(zones, zone_len, hr_max);

  while(true){
    double hr_now;
    printf("Enter the current BPM");
    scanf("%lf",&hr_now);

    if (hr_now <=0) break;

    Zone * cur_zone = find_zone(zones, zones_len, hr_now, hr_max);
    if (cur_zones) print_zone(cur_zone);
    else printf("Cannot find the zone");

    

  }
  
 return 0;
}
