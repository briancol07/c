# Pointers 

## Topics 

* Understand Pointers
* Null pointers
* Constant pointers 
* Functions 

## Overview

* [1-Working-with-Pointers.md](./1-Working-with-Pointers.md)
* [challenges](./challenges)
  * [challenge1.c](./challenges/challenge1.c)
  * [challenge2.c](./challenges/challenge2.c)
* [examples](./examples)
  * [example1.c](./examples/example1.c)
  * [example2.c](./examples/example2.c)
  * [example3.c](./examples/example3.c)
