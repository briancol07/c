# Defining and Accessing pointer

## Declaration 

```c
int *some_ptr;
char *ptr_2;
float *ptr_3;
```
## Why declare the pointer type?

* Pointer arithmetich 
* Type checking : You can only assign the correct data-type
  * Casting 
  * Void pointers 

## Definition

```c
int *some_ptr = NULL;
```
* NULL is equivalent to zero
* NULL is guaranteed not point to nay memory location 
* Defined in stddef.h but included also in stdio.h and stdlib.h 

## Assignment 

```c
int *some_ptr = &data;
```

* The unary address-of operator & gives the address of its operand 
* The data address type should be the same as the pointer type 

## Pointer casting 

```c 
int *some_ptr = (int*) &data;
uint32_t *some_ptr = (uint32_t *) 0x100C;
```
* Convert the data address type to the pointe type
* The second one:
  * Specify that 0x100C is the address of a 32 bit unsigned int 
  * Useful with registers (embedded systems) 

## Void pointer 

```c
void *some_ptr = &data;
```

* Can point to any data-type
* no type cheking ... use with care!

## Indirection 

> A pointer provides an indirect means of accessing the value of a particular data item

```c

int apples =12;
int *ptr = &apples;

// Indirection to get  apples value

int num1 = * ptr;     // num1 = 12 
int num2 = * ptr + 8  //num2 = 20 

// Indirection to set apples value 

*ptr = 99;      // apples = 99
(*ptr)++;       // apples = 100 
```

## Indirection & void pointers 

```c
int apples = 12;
void *ptr = &apples;

// we cant do int num1 = *ptr;
int num1 = *((int *) ptr);    // num1 = 12

```

* You cannot dereference a void pointer
* You must cast a void pointer before indirection 


