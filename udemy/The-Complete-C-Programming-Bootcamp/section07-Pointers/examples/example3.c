#include<stdio.h>
#include<stdlib.h>


int sum (int a , int b){
  return a + b;
}
// being a constant we cant change the value of the address 
int accumulate (int *const accumulator, int  value){
  if (value >0){
    *accumulator += value;
    return 0 ;
  } else return -1
}

// parameter is  symbolic name which enter 
// by value gets a copy first 

void printStudents(int students){
  printf("Students = %d \n",students);
}

void acumulateWrapper (int *const accumulartor , int value ){
  if (accumulate(accumulator, value ){
    printf("warning value = %d \n",value);
  }

}

int main(){
  int girls = 12 , boys = 9;
  int students = sum(girls, boys);

  printStudents(students);
  int class_1 = 28 , class_2 = 19 , class_3 = -1;
  int accumulator = 0; 
 // if (accumulate(&accumulator, class1)) == -1) {
 //   printf("warning");
 // }

  accumulateWrapper(&accumulator,class_1);
  accumulateWrapper(&accumulator,class_2);
  accumulateWrapper(&accumulator,class_3);

  return 0;
}
