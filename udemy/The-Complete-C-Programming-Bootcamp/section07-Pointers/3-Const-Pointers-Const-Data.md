# Constants and Pointers

```c 
const int data = 123;
```

* What can be declared constant?
  * The value of the pointer (the address stored in it)
  * The value pointed to by the pointer 
  * Both 

Constant Pointers | Pointers to constant | Everything Constant
------------------|----------------------|---------------------
int \*const my\_ptr = &some\_data; | const int \*my\_ptr = &some\_data; | const int \*const my\_ptr = &some\data;
The address sotred in the pointer cannot be changed | The value pointed to must not be changed by the pointer | Neither the pointer nor the value it points to can be changed.
