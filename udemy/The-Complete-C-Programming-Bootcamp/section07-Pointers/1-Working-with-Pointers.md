# Working with Pointers 

> Working with pointers means working with addresses 

## Pointers 

* Reference to the data by address 
* The strength and fear of the c programming language 
* Widely used in the embedded field to refer peripheral registers 
* Widely used in system programming in general 

Pointer is a container of the address, change the address change the pointer

* Knowing the address we can:
  * Change the value
  * print the value 

A pointer can point to the address of another pointer.
