// Simple swapper

#include<stdio.h>
#include<stdlib.h>

/************************
 * Build a program that exchange two integer values 
 * Goals : 
 *  * implement the swap operation inside a function 
 *  * The swap function must exchange the value of two int variables declared in main 
 *
 * tips:
 *  - Use pointers!
 *  - Thing carefully about the const keyword
************************/

void swap(int *p1 , int *p2){
  int temp;
  temp = *p2;
  *p2 = *p1;
  *p1 = temp;

}

int main(){

  int a = 12; 
  int b = 34;

  printf("before swapping a = %d , b = %d", a ,b );

  int *pa = &a;
  int *pb = &b;

  swap(pa,pb);
  printf("after swapping a = %d , b = %d", a ,b );

 // swap(&a, &b);
 // we can do this also 

  return 0;
}
