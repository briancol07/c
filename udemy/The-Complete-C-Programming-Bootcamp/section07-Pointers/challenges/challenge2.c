// float decomposition 

#include<stdio.h>
#include<stdlib.h>


/************************************
 *  build a program that decomposes integer and fractional parts of a floating point number 
 *
 *  Goals:
 *    - Write a function called decompose that receives a floating point 
 *    number and returns an interger part (int) and  fractional part float
 *    - Aquire a floating point from the keyboard
 *    - Use the decompose function and print to screen integer and fractional part
 *    - reconstruct the original floating point number and print it 
 *
 *  tips:
 *    - You migh need pointer 
 *
 *
************************************/

void decompose (float number ,int *int_part, float *frac_part){
  *int_part = (int)number;
  * frac_part = number - *int_part;
}


int main (){
  int int_part ;
  float frac_part, input;

  printf("input a floating point number");
  scanf("%f",&input);
  decompose(input,&int_part,&frac_part);
  printf("%.2f , %d ",input,int_part);
  printf("%.2f , %d ",input,frac_part);

  float reconstructed = int_part + frac_part;
  printf("%.2f",reconstructed);
  
  
  return 0;
}
