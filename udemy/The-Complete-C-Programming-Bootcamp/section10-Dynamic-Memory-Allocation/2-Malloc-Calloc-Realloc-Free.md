# Mallor - Calloc - Realoc - Free

## Allcoate memory 

> Allocates size bytes of uninitialized storage 

``` c 
void *malloc(size_t size);
```
## Allocate array of elements 

> Allocates memory for an an array of num objects of size and initializes all bytes in the allocated storage to zero

``` c 
void *calloc(size_t num, size_t size);
```
## Realocate memory 

Reallocates the given area of memory. 

``` c 
void *realloc(void *prt,size_t new_size);

```
## Free memory 

``` c 
void free(void *ptr);
```
