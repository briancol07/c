# Why use dynamic memory allocation

> In memory, where are the variables we use ? 

## Three separate memory pools 

Type | Description
-----|-------------
Static | Global variable storage, permanent for hte entire run of the program 
Stack | Local variables storage (automatic continuous memory, LIFO)
Heap | Dynamic storage (large pool of memory, not allocated in contiguous order)

### Static 

* life cycle as the entrie life of the program 
* Global variables , declared outside functions 
* Static variables , declared inside functions 
* Only one copy of that variable fro the whole program 

### Stack 

* Automatically managed (Compiler)
* Grows and shrinks as LIFO (Last in First out)
* Local variables and parameters of a function
* Life cycle limited to the life cycle of that function
* limited pool of memory 
* Stack overflow may occur! 
* It have like frames (add to stack)

### Heap 

* Managed by the programmer 
* Allocation and the deallocation with special functions 
* Large pool of memory, physical memory bounds 
* Memory blocks managed usign pointers 
* Memory leaks can occur! 

## How to Choose one 

Type | Why 
-----|--------
Static | For memory you know in advance you will always need it throughout the program 
Stack | When you have data you need as long a function is on the stack 
Heap | For maximum flexibility, when you don't know in advance how much room you'll need 
