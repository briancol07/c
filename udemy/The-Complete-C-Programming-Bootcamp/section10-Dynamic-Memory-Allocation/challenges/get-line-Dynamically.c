#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

/*
* Implement a function that acquires a line from the console and has this prototype:
*   char *get_line_alloc(int *plen)
*   
*
*/



char *get_line_alloc(int *plen);

int main(){

  char *line = NULL;
  int len;


  do {
    printf("Enter a line (END to quit)\n");
    free(line); //before calling the function  
    line = get_line_alloc(&len);
    if (line){
      printf("%s (len = %d)\n",line,len);

    }else printf("Cannot allocate memory\n");


  }
  while( line && strcmp(line , "END"));

}

char *get_line_alloc(int *plen){

  if(!plen){ // same as : if plen == NULL
    // The user does not care about the lenght 
    
    plen = malloc(sizeof(*plen)); // malloc allocate the size of the type that *plen is pointing 
    if (!plen){ // cannot allocate 
      return NULL;
    }

  }
  *plen = 0;
  char *line = NULL;
  int buffer_size = 0, input;

  // do it with blocks 
  const int alloc_block_size = 5;

  while(true){
    input = getchar();

    if (buffer_size <= *plen){
      // resize the memory block 
      buffer_size += alloc_block_size;
      char *realloc_line = realloc(line, buffer_size);

      if (realloc_line) {
        free(line); // because realloc don't do it 
        return NULL;
      }
      // reallocation done 
      
      line = realloc_line; 
      
    }

    if (input != '\n' && input != EOF){
      line[(*plen)++] = (char)input;
      //line[*plen++]   --> This is wrong 
    }else break;


  }

  line[*plen] = '\0';
  return line;

}
