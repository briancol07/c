# Dynamic Memory Allocation 

## Topics 

* Understand Dynamic Memory 
* Memory Pools
* Stack / Heap / Static 
* Allocation Functions 

## Overview

* [1-Why-Use-Dynamic-Memory-Allocation.md](./1-Why-Use-Dynamic-Memory-Allocation.md)
* [2-Malloc-Calloc-Realloc-Free.md](./2-Malloc-Calloc-Realloc-Free.md)
* [challenges](./challenges)
  * [get-line-Dynamically.c](./challenges/get-line-Dynamically.c)
