# Goto 

```c
goto identifier;
```

* Immediately jump to identifier label
* Unconditional jump 

## Use Cases 

### Case 1 

> Break out of nested loops 

``` c
for (int i = 0 ; i < max_i ; i++){
  for (int j = 0 ; j < max_j; j++){
    for (int k = 0; k < max_k; k++){
      if (break_condition){
        goto final;
      }
    }
  }
}

final:
```

### Case 2 

> Error handling, cleanup resources at the end of a procedure

```c 
status = do_something();
if (status == ERROR){
  goto cleanup;
}

cleanup:
``` 

## When **NOT** to use goto 

> Jump everywhere, breaking the nomarl decision-making flow. Making Spaghetti code.

```c 

init:

if (test_1) goto procedure_1;
if (test_2) goto procedure_2;

procedure_0:
  if (some_reason) goto procedure_2;

procedure_1:
  goto init;

procedure_2:
  if(nobody_knows_why) goto procedure_0;

``` 
