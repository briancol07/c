# While loop 

```c 
while (test){body} 
```

1. Test condition 
  * if test != 0 -> body executed
  * if test == 0 -> loop terminated 
2. if loop not terminated -> jump to test 

see [example3.c](./examples/example3.c)

## Do While loop

```c 
do {body} while(test)
```

1. Body executed 
2. Test condition 
  * If test != 0 -> jump to body 
  * If text == 0 -> loop terminated 

see [example4.c](./examples/example4.c)
