# If else Statement 

## if Statement 

```c 

if (condition) instruction;

```

* if condition != 0 -> Instruction executed
* If condition == 0 -> Instruction skipped
* Condition can be variable, constnat, experssion, function call

## if else Statement 

```c 

// example 
int a = 0;
int b = 1;

if ( a == b) {
  printf("a equal to b");
} else {
  printf("a not equal to b");
}

```

* Not a good habit to not use { } , if there are no curly brackets it will do the first statement. 

see [example1.c](./examples/example1.c)

## Another way

> Ternary Operator '?' 

```c 
condition ? a:b;
```
* if condition != 0 -> A is evaluated 
* if condition == 0 -> B is evaluated
