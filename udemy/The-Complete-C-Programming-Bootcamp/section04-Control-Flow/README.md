# Control Flow

## Topics 

* If/else
* Switch
* While / for loops 
* Break / continue 
* Goto

## Overview 

* [1-If-Else-Statement.md](./1-If-Else-Statement.md)
* [2-Switch-Statement.md](./2-Switch-Statement.md)
* [3-Loop-While.md](./3-Loop-While.md)
* [4-Loop-For.md](./4-Loop-For.md)
* [5-Break-Continue.md](./5-Break-Continue.md)
* [6-Goto.md](./6-Goto.md)
* [challenges](./challenges)
  * [challenge1.c](./challenges/challenge1.c)
  * [challenge2.c](./challenges/challenge2.c)
  * [challenge3.c](./challenges/challenge3.c)
  * [challenge4.c](./challenges/challenge4.c)
  * [challenge5.c](./challenges/challenge5.c)
  * [challenge6.c](./challenges/challenge6.c)
* [examples](./examples)
  * [example1.c](./examples/example1.c)
  * [example2.c](./examples/example2.c)
  * [example3.c](./examples/example3.c)
  * [example4.c](./examples/example4.c)
