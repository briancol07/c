// Minimum power of 2 

#include<stdio.h>
#include<stdlib.h>

/*******************************************
 * Goals 
 *  - Acquire user input as int 
 *    - print the smallest power of 2 grater than or equal to the number
 *  - If input is negative:
 *    - do nothing 
 *  - Repeat until user enter -1
*******************************************/


int main(){
  int num ;
  int result;
  printf("");
  do {
    printf("Enter a number");
    scanf("%d",&num);
    if (num >0){
      result = 1;
      while(result < num){
        result *= 2; // shift the number by one is like multiplying by 2 result <<= 1;
      }
      printf("Minimum power of 2 grater than %d: %d \n",num,result);

  }
  while(num != -1);
  
  return 0;
}
