// Planetary Alignment 

#include<stdio.h>
#include<stdlib.h>

/***********************************************************
 *  Context:
 *    - Three plantes revolving around a star
 *    - Each orbit takes x units of time
 *    - Unit of itme: one day (86400 seconds)
 *  Goals:
 *    - Calculate how many days elapse between planets linear alignmen
 *    - Calculate how many revolutions each planet makes around the star in this time 
 *  Tips:
 *    - orbits duration: a,b,c (days)-> linear alignment every lcm(a,b,c) days
 *    - each planet completes lcm(a,b,c) / x orbits around the star 
 *  Trace:
 *    - Get the duration(in days) of orbits a,b,c from the user
 *    - calculate lcm (a,b,c):
 *      > Store the maximum between a, b, c
 *      > Iterate no more than 10000 times 
 *      > check lcm (a, b, c) using the stored max and the module operator
 *      > if exceeded the maximum number of iterations: print a warning 
 *      > else: print the result and the number of complete revolution ofr each planet 
 *  Test:
 *    - Calculate Earth-Venus-Mercury linear alignment 
 *    - Caculate Earth-Mars-Jupiter linear alignment 
 *    - Check orbits dutarion: https://spaceplace.nasa.gov/year-on-other-planets/en/
 *
***********************************************************/

int main(){
  int a,b,c;
  int max;

  printf("Enter the duration of orbit a  in days: ");
  scanf("%d",&a);
  printf("Enter the duration of orbit a  in days: ");
  scanf("%d",&b);
  printf("Enter the duration of orbit a  in days: ");
  scanf("%d",&c);

  max = a > b ? a : b;
  max = c > max ? c : max;

  int lcm = -1;
  const int max_iter = 10000;

  for (int i = 1; i < max_iter; i++){
    int mul = max * i;
    if(mul % a == 0 && mul % b == 0 && mul % c == 0){
      lcm = mul;
      break;
    }
  }

  if (lcm == -1){
    printf("Exceeded the maximum number");
  }else{
    printf("Orbits a, b, c align every %d \n",lcm);
  
    printf("Plante a completes %d orbits \n",lcm/a);
    printf("Plante b completes %d orbits \n",lcm/b);
    printf("Plante c completes %d orbits \n",lcm/c);
  }
  return 0;
}


