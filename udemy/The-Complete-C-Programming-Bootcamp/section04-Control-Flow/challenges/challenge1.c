// Even Odd

# include<stdio.h>
# include<stdlib.h>

/*************************************
 * Goals :
 *        promt user to enter a positive integer number 
 *        if number is negative print a warning and exit 
 *        check if number is eve or odd and store the result in a boolean variable using the ternary operator 
 *        print whether the number is even or odd 
 *
*************************************/


int main() {

  int num;
  int a;

  printf("insert a positive number: ");
  scanf("%d",&num);
  if (num < 0){
    printf("Error number is negative");
    // podemos cortar de esta otra manera 
    // return 0;
  }
  else {
    a = num % 2 == 0 ? 1 : 0 ; 
    if (a){
      printf(" a is even");
    }
    else printf(" a is odd");
  }

  return 0;
}
