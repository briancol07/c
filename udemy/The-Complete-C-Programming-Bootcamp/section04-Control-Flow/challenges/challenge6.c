# // Prime Numbers 

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>


/******************************************************************
 * Prime Number: Integer number : grater than 1, only divisible by 1 and itself
 * Goals:
 *  - Find all prime numbers below a given upper limit
 *  - Find the first prime number above a given lower limit
 *  - Write some dummy cleanup code before the final return (simply print somethin)
 *  - Define constatn Max equal to 1000
 *  - If user give an upper limit lower than 2 or greater than max immediately execute the cleanup ocde and exit the program
 *  - Same chek for the lower limit
 * Tips:
 *  - How to che if x is a prima number:
 *    > scan all numbers from 2 to x 
 *    > use the module operator to find out if x is prime
 *
******************************************************************/

int main(){

  const int max = 1000;
  int upperLimit;
  printf("Enter the upper limit: ");
  scanf("%d",&upperLimit);
  
  if (upperLimit < 2 || upperLimit > max){
    printf("Error: must be 2 < limit < max \n");
    // Cleanup and exit 
    goto exit_program;

  }

  printf("Prime numbers up to %d: \n:",upperLimit);
  for (int number = 2; number < upperLimit; number++){
    bool isPrime = true;
    for(int i = 0; i < number ;i++){
      if(number % i == 0){
        isPrime = false;
        break;
      }
    }
    if(isPrime){
      printf("%d \n",number);
    }
  }

  // Find the first prime number above lowerLimit
  
  int lowerLimit;
  printf("Enter the lower limit: ");
  scanf("%d",&lowerLimit);
  if (lowerLimit < 2 || lowerLimit > max){
    printf("error must be 2 < limit < Max \n");
    goto exit_program;
  }

  int firstPrimeNumber = -1 ;

  for (int number = lowerLimit; number < max;number++){
    bool isPrime = true;
    for(int i = 2; i < number ; i ++){
      if (number %i == 0){
        isPrime = false;
        break
      }
    }
    if(isPrime){
      firstPrimeNumber =number;
      break;
    }
  }
  if (firstPrimeNumber == -1){
    printf("Cannot find prime number:");
  }else {
    printf("The first prime number is %d",firstPrimeNumber);
  }


exit_program:
  printf("Some dummy cleanup code..");

  return 0;
}
