// Sigma drawing


#include<stdio.h>
#include<stdlib.h>

// Draw a sigma iwht base = 10, tip width = 5, total height = 11
//


int main(){
  const char symbol = 'x';
  const int base_width = 10;
  const int tip_width = 5;
  for (int i =0; i < base_width;i++){
    printf("%c",symbol);
  }
  for (int i = 1; i <tip_width ; i++){
    for (int j = 0 ; j < i ; j++){
      printf(" ");
    }
      printf("%c \n",symbol);
  }

  for (int i = tip_width; i > 0 ; i-- )
    for (int j = 0 ; j < i ; i++){
      printf(" ");
    }
      printf("%c \n",symbol);
  }

  for (int i =0; i < base_width;i++){
    printf("%c",symbol);
  }
  return 0;
}
