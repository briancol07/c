# For Loop

```c 
for (initialization; test; update){body}
for (int i = 0; i < 5 ; i ++){printf("%d",i};}
```

1. Initialization executed once 
2. Test condition
  * If test != 0 -> body executed
  * If test == 0 -> loop terminated 
3. If loop no terminated -> update 

you can create multiple initialization and multiple updates in one for 


```c 
for( int i = 0, j = 5; i < j; i++ , j--)
```

It optional to write initialization , test and update you can omit them but be careful, you can make an inifite loop



