# Break and Continue 

> Both skip part of code 

## break

  * Ends the loop immediately
  * Unconditional jump 

```c 
while (test_condition){
  if (break_condition){
    break;
  }
}

``` 

## continue 

* Jump to the end of the loop body 
* Unconditional jump 

```c

while (test_condition){
  if (continue_condition){
    continue;
  }
}


