# Switch Statement 

> list of cases, if nothing occurr we can go to the default 


```c 

switch(expression){
  case constant_1:
    break;
  case constant_2:
    break;
  default:
    break;
}

```

* Expression is evaluated, then it's compared with the value of each case
* Executed the code in the matching case 
* Expression has integer type 
* Case constants have integer type 
* The default clause is optional 
* Don't forget the break statement! 

see [example2.c](./example/example2.c)
