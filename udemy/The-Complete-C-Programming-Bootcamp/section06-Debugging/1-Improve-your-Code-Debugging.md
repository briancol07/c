# Improve your code with debbugging 

## Why Debugging ?

* Create a program that works
* Maintenance is expensive 
* Avoid costly breakdowns 


> Debugging is the art of finding problems 


> Debugging is like being the detective in a crime movie where you are also the murderer, and moreover you are also the victim.
> Felipe Fortes 

## Good practices for easy Debugging 

* Using meaningful names 
* Good planning and clear documentation 
* Break the problem into smaller, isolated chunks
* Keep it Simple, and focus on what matters
* Take it one step at a time, test every single step!
* Unit Testing: Test Driven Development 
* Use printf wisely 
* Create log files
