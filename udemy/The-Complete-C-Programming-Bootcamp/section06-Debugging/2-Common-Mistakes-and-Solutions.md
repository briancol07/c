# Common Mistakes and Solutions 

## Common Mistakes 

* Sintax Error (e.g. forgetting ;)
* Typos (e.g. using = instead of == )
* Logic Errors
* Misundertanding of C
* Unexpected Conversions 
* Segmentation Fault
* Memory Corruption 
* Poor code isolation 

## How to solve Problems 

* Reproduce the problem
* Identify and isolate the problem
* Simplify the problem
* Compare the program with previous working versions

> Once you understand the problem, the solution is close! 

## Debugging Tools

* Print in the terminal 
* Debuggers 
* Log Files
* Analyze memory dump and call stack 
* Static Analyzers 
