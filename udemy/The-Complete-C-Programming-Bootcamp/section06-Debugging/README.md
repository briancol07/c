# Debugging 

## Topics 

* How to debug
* Debugging process 
* VSC debugger
* C common mistakes 

## Overview

* [1-Improve-your-Code-Debugging.md](./1-Improve-your-Code-Debugging.md)
* [2-Common-Mistakes-and-Solutions.md](./2-Common-Mistakes-and-Solutions.md)


