# Section 3 

## Topics 

* c operators 
* bitmasks 
* casting 
* challenges

## Overview 

* [1-Arithmetic-Operators.md](1-Arithmetic-Operators.md)
* [2-Increment-Decrement-Operators.md](2-Increment-Decrement-Operators.md)
* [3-Relational-Operator.md](3-Relational-Operator.md)
* [4-Logical-Operators.md](4-Logical-Operators.md)
* [5-Bitwise-Operators.md](5-Bitwise-Operators.md)
* [6-Bitmasks.md](6-Bitmasks.md)
* [7-Bit-Manipulation.md](7-Bit-Manipulation.md)
* [8-Assignment-Operators.md](8-Assignment-Operators.md)
* [9-The-size-of-Operator.md](9-The-size-of-Operator.md)
* [10-Type-Conversion.md](10-Type-Conversion.md)
* [11-Other-Operators.md](11-Other-Operators.md)
* [12-Operators-Precedence.md](12-Operators-Precedence.md)
* [challenge](challenges)
  * [challenge1.c](./challenges/challenge1.c)
  * [challenge2.c](./challenges/challenge2.c)
* [examples](examples)
  * [example1.c](./examples/example1.c)
  * [example2.c](./examples/example2.c)
  * [example3.c](./examples/example3.c)
  * [example4.c](./examples/example4.c)
  * [example5.c](./examples/example5.c)


