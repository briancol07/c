# Logical Operators 


Operator | Boolean Algebra | Boolean Symbol
:-------:|:---------------:|:--------------:
&& | AND | $` \land `$
|| | OR  | $` \lor  `$
!  | NOT | $` \neg  `$

## Truth Table 

 x | y | x AND y | x OR y | NOT x
:-:|:-:|:-------:|:------:|:----:
0 | 0 | 0 | 0 | 1 
1 | 0 | 0 | 1 | 0 
0 | 1 | 0 | 1 | 1
1 | 1 | 1 | 1 | 0


