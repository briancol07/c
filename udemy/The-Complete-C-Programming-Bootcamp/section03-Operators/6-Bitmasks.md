# Bitmasks

Manipula registers 

* With bitwise and 
  * Bit clearing 
  * Bit Testing 
* Or 
  * Bit Setting 
* Xor 
  * Bit toggling 
    * switch bits 


Operator | Bitmask | Description 
:-------:|:-------:|:-----------:
& | Bit Clearing | Mask is 0 => Result is 0 
& | Bit Testing  | Mask is 1 -> Input pass
\| | Bit Setting | Mask is 1 -> Result is 1 
^ | Bit Toggling | Mask is 1 -> Input toggle 

