# Bitwise Operators 

## Raw Bits Manipulation

* Embedded C Programming 
* Telecommunicaitons SW 
* Device Drivers 

## Bitwise Operators 

Operator | Description 
:-------:|:-----------:
& | Bitwise AND 
| | Bitwise OR
^ | Bitwise XOR
~ | Bitwise complement 
\<< | Shift left 
\>> | Shift right  

* Logical : boolean operation on the whole number
* Bitwise : boolean operation on each raw bit 

## Shifted Values 

``` 

A << N --> A x 2 ^ N

1 << 0 --> 1 (A)
1 << 1 --> 2 (Ax2)
1 << 2 --> 4 (Ax2x2)
1 << 3 --> 8 (Ax2x2x2)

A >> N --> A / 2 ^ N 

8 >> 0 --> 8 (A) 
8 >> 1 --> 4 (A/2) 
8 >> 2 --> 2 (A/2/2) 
8 >> 3 --> 1 (A/2/2/2) 

```

See [example3.c](./examples/example3.c)


