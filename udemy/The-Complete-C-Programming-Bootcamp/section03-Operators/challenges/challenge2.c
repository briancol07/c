#include<stdio.h>
#include<stdlib.h>

/***************************************************
 * Bit Selector 
 * - Goal
 *   - N, M design variables 
 *   - make a selector that selects the bits [N:M] included 
 *   - print data and result in hexadecimal format, 4 digits wide, zero padding
 *   - check these selection ranges 
 *    -[0:3] expected result 0x000D
 *    -[4:7] expected result 0x000C
 *    -[8:11] expected result 0x000B
 *    -[12:15] expected result 0x000A
 *
***************************************************/

int main(){
  unsigned data = 0xABCD;
  unsigned n = 4;
  unsigned m = 7;

  /**
   * 0b1111 = 15 = 2^4 -1 
   * 0b0111 = 7 = 2^3 -1 
   * 0b0011 = 3 = 2^2 -1 
   * 0b0001 = 1 = 2^1 -1 
   * 
   * w : width of the bitmask in bits 
   * bitmask = 2^w -1 
   * [0:1] --> w = 2 bits 
   * [0:2] --> w = 3bits 
   * [n:m] --> w = m - n + 1 
   * 2^w = 1 << w
   * bitmask = 2^w -1 = (1 << w) -1 
   */

  unsigned w = m - n + 1 ;
  unsigned bitmask = (1 << w)-1;
//  unsigned result = (data >> n) & 0b1111;

  unsigned result = (data >> n) & bitmask;

  printf("data ---> 0x%04X \n",data);
  printf("result ---> 0x%04X \n",result);
  return 0;
}
