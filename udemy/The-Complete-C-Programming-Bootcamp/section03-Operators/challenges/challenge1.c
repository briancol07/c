#include<stdio.h>
#include<stdlib.h>


/***********************************************
 * Calculate suface and volume of a sphere 
 * Goals :
 *  - Prompt user to enter the radius
 *  - Calculate the surface area 
 *  - Calculate the enclosed volume
 *  - print the radius and the results in decimal format ( 2 digit of precision) 
 *  - Print the radius and the results in scientific notation (3 digits of precision)
 *
***********************************************/

int main (){
// A = 

  const double pi = 3.141592;
  double r; 
  printf("Enter de radius");
  scanf("%1f",&r);

  double surface = 4 * pi * r * r;
  double volume = 4.0 / 3 * pi * r * r *r;
  printf("Decimal");
  printf("radius : %.2f\n",r);
  printf("Surface %.2f\n",surface);
  printf("Volume = %.2f\n",volume);

  printf("Scientific");
  printf("radius : %.3e\n",r);
  printf("Surface %.3e\n",surface);
  printf("Volume = %.3e\n",volume);
  return 0;
}


