# Other operators 


Operator | Description    
:-------:|:-----------:
a = a ? b : c | Ternary Operator
b = &a | Reference Operator 
\*b = a | Dereference Operator 
b = a[x] | Array reference operator 
b.x = a | Member Selection Operator 
b-\>x = b | Member Selection Operator (pointers)
