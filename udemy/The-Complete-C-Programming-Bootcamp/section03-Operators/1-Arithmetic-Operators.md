# Arithmetic Operators 

It's a simbol representing a mathematical or logical operation

## Operator 

High level operation undestandable 

## Arithmetic operators 

Allow us to carry out mathematical operations.

### Binary Operators 

```
C = A [operator] B 
```

Operator | Description | Examples 
:-------:|:-----------:|:-------:
+| Add two operands | A + B = 15
-| Subtract second operand from first | A - B = 9
\* | Multiplies two operands | A * B = 36
/ | Divides first operand by second | A / B = 4
% | Modulo operator: remainder after division | A % B = 0 


see [example1.c](./examples/example1.c)
