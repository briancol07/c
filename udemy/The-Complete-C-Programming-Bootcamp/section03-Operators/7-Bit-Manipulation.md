# Bit manipulation 

## Setting Nth Bit 

┌───────────────────┐
│result = data |mask│
└───────────────────┘

```
e.g. set bit [6]

        76543210

data    10101010
|
mask    01000000
=      ----------
result  11101010

Code:

result = data | 0b01000000;

Better code :

result = data | (1 << 6);

```

## Clearing Nth bit

┌────────────────────┐
│result = data & mask│
└────────────────────┘

```
e.g. clear bit [6]

        76543210

data    10101010
&
mask    11011111
=      ----------
result  10001010

Code:

result = data & 0b11011111;

Better code :

result = data & ~(1 << 5);

```

## Selecting bits 

```
e.g. sel bit [3:5]

        76543210

data    11101001
>>3     00011101
&       00000111
=      ----------
result  00000101

Code:

result = (data >> 3) & 0b111;

```
