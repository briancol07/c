# The Size of operator 

``` 
sizeof(something)
```

* Size of something in bytes 
* Known in compile-time 
* result is an integer constant 
* something can be a variable, a basic or a derived data type, an expression 

```c

size_t size = sizeof(int);
```

* size\_t is implementation-defined 

see [example4.c](./examples/example4.c) 


