# Casting and Implicit conversions 

> c is strongly-typed 

## Type conversions


```
(type) expression 

```

* Conver expression data type to type 
* Conversion preseves the sign 
* Information may be lost 
* truncation toward zero


```c 

int intNumber = (int)charNumber;
char charNumber = (char)intNumber;
int intNumber = (int)floatNumber
```

if int number is greater tha nonehundred and twenty seven or less, then minus one hundred and twenty eight we have a lot of information we simply cannot transfer

## Integer Promotion 

The compiler will convert into a integer.

see [example5.c](./examples/example5.c)

## Wider Data Type 

> Implicit conversion

```c 

short int a = 123;
long int b ;
b = a ;
```

## Arithmetic Conversion Hierarchy 

* Long Double 
* Double 
* Float 
* unsigned long long int 
* long long int 
* unsigned long int 
* long int 
* unsigned int 
* int 

upper is promotion made by compiler and if you want lower you have to make it explicit 
