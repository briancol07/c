# Assingment operators 

```c 

//          -> Assignment operator
//          |
//          |
temperature = 75;

```

produces a result 

Operator | Same as | Example 
:-------:|:-------:|:-------:
+= | a = a + b | a += b 
-= | a = a - b | a -= b 
\*= | a = a \* b | a \*=b
/= | a = a / b | a /= b 
%= | a = a % b | a %= b 
&= | a = a & b | a &= b
|= | a = a \| b | a \|= b 
^= | a = a ^ b | a ^= b 
>>= | a = a >> b | a >>= b 
<<= | a = a << b | a <<= b 



