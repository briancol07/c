#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

int main () {
  uint8_t a = 12; // 0000 1100
  uint8_t b = 5 ; // 0000 0101


  printf(" A & B = %u \n", a & b); // a & b = 4
  printf(" A | B = %u \n", a | b); // a & b = 13
  printf(" A ^ B = %u \n", a ^ b); // a ^ b = 9

  printf(" A << 1 = %u \n", a << 1U); // a << 24
  printf(" A >> 1 = %u \n", a >> 1U); // a >> 6

  return 0;
}

