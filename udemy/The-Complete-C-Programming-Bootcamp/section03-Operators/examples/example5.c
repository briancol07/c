#include<stdio.h>

int main(){
  char a = 100, b = 5, c = 10;
  char d = (a * b) / c ;

  printf(" d = %d",d);

  char x = 0xFF; // -1 
  unsigned char v = 0xFF; // 255
  printf("x == v --> %d",x == v);

  // Arithmetic Conversion 
  

  int y = 1;
  long int s = 2;
  double p = 3.3;

  s = s + y;  // implicit conversion: y is promoted to long int
  p *= s;     // implicit conversion: s is promoted to double 
  
  return 0;

}
