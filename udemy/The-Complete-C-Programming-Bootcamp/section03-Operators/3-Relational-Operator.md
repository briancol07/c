# Relational operators

```
A [relation] B 

```

* Evaluated to 1 if true 
* Evaluated to 0 if false

Operator | Description | Example 
:-------:|:-----------:|:-------: 
\== | Equal to | A == B 
!=  | Not equal to | A != B 
\>  | Greater than | A > B 
\<  | Less than | A < B 
\>= | Greater than or equal to | A >= B
\<= | Less than or equal to | A <= B 
