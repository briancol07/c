# Increment and Decrement Operators  

```
B = A [operator]
```

## Unary Operators 

Operator | Description
:-------:|:----------:
++ | Increment operand by 1
-- | Decrement operand by 1 


## Post Increment / Decrement 

``` 
// Case A 

B = A++
B = A -- 

// Case B 

B = ++A 
B = --A

```
* Case A 
  1. Uses the value of A in the expression
  2. Increment / Decrement A 
* Case B  
  1. Increment / Decrement A 
  2. Uses the value of A in the expression

See [example2.c](./examples/example2.c)
