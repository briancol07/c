## Function call

```c 
function name(arg1, arg2, ...);
```

* Function call must match parameter list and return type
* Same order of parameters
* Return value can be ignored 

see [example1.c](./examples/example1.c)

## Function Declaration

```c 
return_type function_name(parameter list);
``` 

* Tells the compiles how to call the function
* Optional, but useful for:
  * Exposing some functions in a header file 
  * calling a funciton before defining it 

see [example2.c](./examples/example2.c)

## Naming Rules 

> Choose meaningful names ! 

* Only letters ( upper and lower case), digits (0-9) and underscore (\_)
* The first character can't be a digit 
* Avoid leading underscore 
* Cannot use reserved words 
* Cannot use the same name as a previously declared functions
* valid names:
  * set\_led
  * getData
  * calc\_rand
  * init\_com
  * isEqual

