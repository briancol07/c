# Creating Fucntions

## main 

When the "bootloarder" end doing their things call our main function

```c

int main() {.... return 0;}
```


## Function Definition

```c 

return_type function_name(type1 id1, type2 id2, ...){
  /* function body */
  return expression;
}

```

* Parameter list can be empty 
* Each function must have different name
* If return_type is void, the function does not return any values 
* Return expression is mandatory if return_type is not void 

