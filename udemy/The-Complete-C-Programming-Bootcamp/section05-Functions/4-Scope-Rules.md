# Scope Rules

> Scope means visibility, be able to access by our code 

* File Scope 
  * All the file 
* Block Scope 
  * Only visible in that block 
* Types 
  * Local variables 
  * Global variables 
  * Parameters 

## Local Variables 

* Block scope 
* Visibility limited to the function
  * Each time the program enter the function, it create,use and destroy them

## Parameters 

* Block Scope
* Visibility limited to the function 
* Declare in the function definition 

## Global Variables

* File scope 
* Visibility extended to the whole file 
* Visible to all of the functions

## Visibility Rules 

* A local variable hides a global variable with the same name 
* a block variable hides local and global variable with same name




