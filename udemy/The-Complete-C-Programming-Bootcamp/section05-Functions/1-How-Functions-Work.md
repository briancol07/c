# How Functions work

## What are Functions?

* It's a block of code that performs a specific task 
* You can invoke that block of code by making a function call
* A function can be used to execute and action, e.g. printf("abc")
* A function can be used to obtain a result e.g. letter = tolower('A')
* A function can be used to change something e.g. increment(&count)
* Function allow you to encapsulate pieces of code, hiding them behind the function name

> Keep it Simple and focus on what matters 

## Why use Functions ?

* Dividing problem into smaller chunks
* Simplify a problem with the black box approach 
* Improved codee readability 
* Avoid code duplication 
* Only one place to make changes
* Isolating code blocks means isolating bugs 
* Independent development, testing and debugging 
* allow the reuse of already written code 

