# Functions 

##  Topics 

* How functions works
* Create Functions 
* Scope rules 
* LOcal and global

## Overview

* [1-How-Functions-Work.md](./1-How-Functions-Work.md)
* [2-Creating-Functions.md](./2-Creating-Functions.md)
* [3-Function-Calls.md](./3-Function-Calls.md)
* [4-Scope-Rules.md](./4-Scope-Rules.md)
* [challenges](./challenges)
  * [challenge1.c](./challenge1.c)
* [examples](./examples)
  * [example1.c](./examples/example1.c)
  * [example2.c](./examples/example2.c)
