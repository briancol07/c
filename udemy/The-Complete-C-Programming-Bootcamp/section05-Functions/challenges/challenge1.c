// Prime Numbers improved 
// same as one we have done 
#include<stdio.h>
#include<stdlib.h>

bool isOutOfBounds(int);
void printLimitError();
bool isPrime(int);

const int max = 1000;


int main(){
  
  int upperLimit;
  printf("Entre the upper limit");
  scanf("%d",&upperLimit);
  
  if(isOutOfBounds(upperLimit)){
    printLimitError();
    return 0;
  }


  printf("Prime number up to %d \n",upperLimit);
  for(int number = 2; number < upperLimit; number++){
    if(isPrime(number)){
      printf("%d \n",number);

        }
  }

  int lowerLimit;
  printf("Enter the lower limit: ");
  scanf("%d",&lowerLimit);

  if(isOutOfBounds(lowerLimit)){
    printLimitError();
    return 0;
  }

  int firstPrimeNumber = -1;
  for (int number = lowerLimit; number < max; number++){
    if (isPrime(number)){
      firstPrimeNumber = number;
      break;
    }
  }

  if (firstPrimeNumber == -1){
    printf("Cannot find prime number");
  }else {
    printf("First prime number above %d is %d \n", lowerLimit, firstPrimeNumber);
  }


  return 0;
}

bool isOutOfBounds(int limit){
  return (limit < 2 || limit > max);
}

void printLimitError(){
  printf("Error: must be 2 < limit < Max \n");
}

bool isPrime(int number){
  bool prime = true;
  for (int i =2 ; i < number, i++){
    if (number % i == 0){
      return  false
    }
  }
  return true;
}
