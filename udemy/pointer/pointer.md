# Pointers 

The variables has a unique address that store the value, to get that address use &variable.

So the pointers are variables that store the address of another variable.

## How to denote a pointer variable ?

```c 
int *a;  
int y = 100 ;
a = &y

// a = y do not work 

``` 

## Types of pointers

* ptr = &var 

* Null pointer
  * Do not asigned any address 
  * int \*ptr = null;
* Void pointer 
  * No datatype 
  * Advantage : can hold any address 
  * void \*ptr 
* wild pointer 
  * unitiliased pointer 
* Dangling pointer 

## Array pointer 

```c


