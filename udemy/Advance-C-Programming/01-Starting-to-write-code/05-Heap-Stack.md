# Heap and Stack Memmory allocation 

> Each type of memory determines where and how it is stored 

## Stack 

* The stack stores temporary variables
  * Used to store variables that are created inside of a function 
  * Easier to keep track of because the memory is only locally available in the function
* LIFO (last input first output) 
  * Linear data structure 
  * There is no need to manage the memory yourself 
  * Variables are allocated and freed automatically 
* Grows and shrinks as variables are created and destroyed inside a function 
  * Every time a function declares a new variable, it is "pushed" onto the stack 
  * Every time a functions exist, all of the variables pushed onto the stack by that function are freed (deleted)
  * Once a stack variable is freed ,that region of memory becomes available for other stack variable
* There is a limit on the size of variables that can be stored 
* Very fast access 
* do not have to explicitly de-allocate variables 
* space is managed efficiently by CPU 
  * Memory is allocated in a contiguous block 
  * Memory will not become fragmented 

## Heap 

* Hierarchical data structure 
* Large pool of memory that can be used dynamically 
* Memory is not automatically managed 
  * More free-floating region of memory 
* THe heap is managed by the programmer 
  * The memory is accessed through the use of pointers
    * Malloc()
    * Free()
  * Failure to free the memory when you are finished with it will result in memory leaks
* there are generally no restrictions on the size of the heap ( or the variable it creates)
* Variables created on the heap are accessible anywhere in the program 
* (relatively) slower access 

## When should I use the heap or stack? 

* Use heap 
  * When you need to allocate a large block of memory 
    * Large array
    * Big struct 
  * When you need to keep that variable around a long time
  * when you need variables chnage size dynamically 
* Use the stack
  * When you are dealing with relative small variables that only need to persist as long as thefunvtionusing them is alive 
    * Easier and faster 
