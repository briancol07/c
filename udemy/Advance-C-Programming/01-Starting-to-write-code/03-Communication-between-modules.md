# Communicatin between modules

Methods can be used so that the modules contained in separate files can effectively communicate.

> Include prototype declaration

## External variables 

Functions contained in separate files can communicate throughh external variables, an extension to the concept of the global variable. This external variable can be change by naother modules (files). 

The variable data type is precceded with the keyword extern in the declaration. 

```c
extern int moveNumber;
```

## Static 

You want a global but no external, you define a global variable to be local to a particular module (file), If no functions other than those contained inside a particualr file need access to the particular variable. In this situation we difine the variable to be static.

If the below declaration is made outside of any function, make the value of the variable accesible from any subsequent point in the file in which the definition appears 

```c
static int moveNumber =0;
```
