# Starting to Write code 

* Create one executable 

Compile source file 

When you make a clean you are deleting all .o files and starting from scratch  

build , will grab all the files and create .o 

## Overview 

* Breaking down a program in multiple 
* Make files
* headers 

> You need to divide the problem into multiple sub problems and then try to tacke it one by one 

## Compile from CLI

This will compile the main.c with other.c and create an output file called main

``` bash
gcc main.c other.c -o main

# extra to compile all c files 

gcc *.c -o main 
```

## How to divide? 

* Each of these sections might be implemented as one or more functions 
* All function from each section will usually live in a single file 
* The file contains the definition of each function
    * Should create a header file for each of the C files 
    * Will have the same name as the C file 
