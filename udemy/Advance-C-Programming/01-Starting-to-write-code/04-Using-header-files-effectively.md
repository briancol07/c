# Using Header files effectively

* Grouping all your commonly used definitions 
* You can then simply include the file in any program that needs to use those definitions
* Standarization 
  * Each programmer is using the same definitions, which have the same values
  * Each programmer is also spared the time-comsuming and error-prone task of typing these definitions into each file that must use them 
* Eliminate the error that cause by two modules that use different definitions for the same data structure 
* The change only has to be done in one file 
