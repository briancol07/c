# Compiling multiple source files From the command line 

* You have 
  * mod1.c
  * mod2.c
  * main.c

To tell the system that these three modules actually belong to the same program, You include the names of all three files when you enter the command to compile the program

``` bash
gcc mod1.c mod2.c main.c -o dbtest
```

> Errors are separately identified by the compiler

you can compile each module separately and using the -c command line option 

* Option tells the compiler not to link your file 
* Does not produce an executable 
* Retains the intermediate object file that it creates 

``` bash 
gcc -c mod1.c
gcc -c mod2.c
gcc -c main.c
```

## Link the compile files 

```bash
gcc mod1.o mod2.o main.o -o main 
```
