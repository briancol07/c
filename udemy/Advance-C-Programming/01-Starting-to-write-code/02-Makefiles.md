# Makefiles 

If you still want to work from the command line, the make file utility is a tool thatyou might want to learn how to use 

* Not part of C language 
* Very helpful when developing larger programs 
* Can help speed your development time 

The make program automatically recomplies files ony when necessary, based on the modification times of a file.

You can even specify source files taht depend on headers files. Specify that a module calle datefuncs.o is dependent on its source file datefunc.c as well as the header file date.h.
If you change anything inside the date.h header file, the make utility automtically recompiles the datefuncs.c file, based on the fact that the header file is newer than the source file.

## Example 

Notation | Meaning 
---------|---------
SRC | Source file to compile 
OBJ | Object files 
PROG| Says what to compile 
$ | Indicates a variable
: | Defines some dependencies ($(PROG):$(OBJ))

With the dependencies , it says that the excuatable is dependent on the object files, if one or more object files change, the executable needs to be rebuilt, must be typed with a leading tab (gcc $(OBJ) -o $(PROG))

The last line , says that each object file depends on its corresponding source file, if a source file changes, its correponding object file must be rebuilt the make utility has built-in rules that tell it how to do that 

```
SRC = mod1.c mod2.c main.c

OBJ = mod1.o mod2.o main.o

PROG = myProgram$(PROG):$(OBJ)
  gcc $(OBJ) -o $(PROG)

$(OBJ):$(SRC)

```

## How to execute make?

You only have to write make in the terminal and he will find the files an do all the things. Make compiled each individual source file and then linked the resulting object files to creat the executable.

```bash 
make
```
