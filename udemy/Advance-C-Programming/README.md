# Advance C Programming course

## Topics 

* Storage Classes 
  * auto, register, static and extern 
* Advanced data Types
  * typdef, variable length arrays, flexible array numbers, complex number types 
* Type qualifiers 
  * const, volatile and restrict 
* Bit manipulation 
  * Binary numbers and bits 
  * Bitwise operators (logical and shifting)
  * bitmasks and bitfields 
* Advanced control flow 
  * goto, null, comma operator
  * stjmp and longjmp 
* more on Input and Output
  * getchar, putchar, fgets, etc
  * puts, sprint, fprintf, fflush
* Advance function concepts 
  * variadic functions ( variable number of arguments)
  * Recursive functions 
  * Inline functions 
* Unions 
  * Overview, defining and accessing union members 
* Advanced preprocessor concepts 
  * \#define, \#pragma, \#error, \#, \#\#
  * Conditional compilation (\#ifdef, \#endif,\#else, \#elif,\#undef, etc)
  * Include guards 
* Macros 
  * overview (vs. functions, when to use)
  * predefined macros
  * creating your own macros 
* Advanced debugging and compiler flags 
  * debugging with the pre-processor, mor on gdb
  * Core files , getting the stack trace 
  * Static analysis and profiling 
* Static libraries and shared object 
  * overview, creation, dynamic loading 
* Working with larger programs 
  * Dividing your program into multiple files and compuling multiple files
* Advanced pointers 
  * Double pointer (pointers to pointers)
  * Funciton pointers
  * more on void pointers
* Useful C Libraries
  * the assert library 
  * General utilities library (stdlib.h), (exit atexit, qsort, memcpy, abort)
  * Date and time functions 
* Data structures 
  * Linked lists, stacks queues and trees
* Inter-process communications (unix based using Cygwin)
  * overview (message queues, shared memory, piping)
  * Forking and signals 
* Threads (pthread(posi), not \<thereads.h\> from c11)
  * overview, creating a thread 
  * mutexes and semaphores
  * tread management (multi-threading, join, detach)
* Networking (unix based using Cygwin)
  * overview (client/server model)
  * creating server and client sockets 

## Using the command line 

* A text edito 
* Command-prompt or terminal window 
* Installed C compiler
* No need for IDE 
* Simple, efficient workflow 
* Better as you gain experience
* Can be used if you are overwhelmed by IDEs 
