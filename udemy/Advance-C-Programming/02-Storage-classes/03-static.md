# Static 

* The static storage class can be used on local and global variables, as well as functions 
* When applied to local variables it instruct the compiler to keep the variable in existence during the life-time of the program 
* When applied to global variables, the static modifies causes that variable's scope to be retricted to the file in which it is declared 
* When applied to functions, the static function can be called only from within the same file as the functions appears 

## Automatic and static variables (local statics) 

* We know that when you normally declare a local variable inside a function 
  * you are declaring automatic local variables 
  * Recall that the keyword auto can in fact, precede the declararion of such variables (optional) 
  * An automatic variable is actually created each time the function is called and destroyed upon exit of the function 
* Automatic local variables can be given initial values 
  * The value of expression is calculated and assigned to the automatic local variable each time the function is called 
  * Because an automatic variable disappears after the funcion complestes excution, the value of that variable disappears along with it 
  * THe value an automatic variable has when a function finishes execution is guaranteed not to exist the next time the function is called 
* Static variables have a property of preserving their value even after they are aout of their scope 
  * Static variables preserve the value of their last use in their scope
  * No new memory is allocated because they are not re-declared 
  * Their scope is local to the function to which they were defined 
* Making local variables static allows them to maintain their values between functions calls
  * Does not create and destroy the local variable each tie it comes into and goes out of scope 
  
## Initialization of local statics 

* A static, local variable is initialized only once at the start of overall program execution 
  * Not each time that the function is called 
* The initial value specified for a static variable must be as simple constant or constant expression
* Static variables also have defaul initial value of zero, unlike automatic variables, which have no default initial value
* Static variable are allocated memory on the heap, not on the stack 

## Static VS Extern variables (global statics) 

* THere are situations that arise in which you want to define a variable to be global but not external 
* You want to define a global variable to be local to a particular module (file)
  * It makes sense to want to define a variable this way if no functions other than those contained inside a particular file need access to the particular variable
* In these situations, you should define the variable to be static 
  * by default they are assigned the value 0 by the compiler 
* If the below declaration is made outside of any function
  * Makes the value of the variable accessible from any subsequent point in the file which the definition appears 
    * Not from functions contained in other files 
* If you need to define a global variable whose value does not have to be accessed from another file 
  * Declare the variable to be static 
  * A cleaner approach to programming 
* The static declaration more accurately reflects the variable's usage 
  * No conflicts can be created by two modules that unknowingly both use different external global variables of the same name 

## Static and structures 

* Static variables should not be declared inside a structure 
* The C compiler requires the entire structure elemetns to be placed together 
  * Memory allocation for structure members should be contiguous 
* It is possible to declare a structure
  * Inside a function (stack segments) 
  * Allocate memory dynamically (heap segment) 
  * It can be even global
* Whatever might be the case, all structure members should reside in the same memory segment 
  * The value for the structure element is fetched by counting the offset of the elemtn from the beginning address of the structure 
* Separating out one member alone to a data segment defeats the purpose of a static variable
* It is possible to have an entire structure as static 
