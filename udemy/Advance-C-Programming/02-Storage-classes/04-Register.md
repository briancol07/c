# Register 

* A processor register (CPU register) is one of a small set of data holding places that are part of the computer processor 
  * A register may hold an instruction, a storage address, or any kind of data 
* The register storage class is used to define local variables that should be stored in a register instead of RAM
  * Makes the use of register variables to be much faster than that of the variables stored in memory during the runtime of the program
* THe register storage class should only be used for variables that require quick access 
  * THe variables which are most frequently used in a c program
  * if a fuction uses a particular variable heavily 
* The key word register hints to the compiler that a given variable can be put in a register 
  * It is the compiler's choice to put it in a register or not 
  * Might be stored in a register depending on hardware and implementation restrictions 
  * Generally, compilers themselves do optimizations and put the variables in register 

## Overview 

* The keyword register is udes to define register storage class 
* Both local variables and formal parameters can be declared as register variables 
* THis storage class declares register variables which have the same functionality as that of the auto variables 
  * the liftime of register variable remains only when control is within the block 
* THe variable stored in a register has a maximum size equal to the register size 
* You cannot obtain the address of a register variable using pointers 
  * Cannot have the unary & operator applird to it 

## Summary 


| Storage Class | Declaration location | Scope(visibility) | LifeTime(alive) |
|:-------------:|:--------------------:|:-----------------:|:---------------:|
Auto  | inside a function| within the function/block| until the function/block completes           |
Register | Inside a function/block| withing the function/block |  until the function/block completes |
Extern | Outside all functions| Entire file plus other files wher the variable is declared as extern |until the program terminates |
static (local) | Inside a function/block |within the function/block| until the program terminates  |
Static (global)| Outside all functions|Entire file in which it is declared | until the program terminates |
