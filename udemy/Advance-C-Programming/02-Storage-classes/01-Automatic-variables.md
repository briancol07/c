# Auto Variables 

## Storage Classes 

Storage classes are used to describe the features of a variable/function
* Include the scope, visibility and life-time
* Help us to trace the existence of a particular variable during the runtime of a program

The life time of a variable is the time period during which variable exist in computer memory 
* Some exist briefly, some are repeatedly created and destroyed, and others exist for the entire execution of a program 

The scope of the variable is where the variable can be referenced in a program. Some can be referenced throughout a program, other from only portions of a program

A varaible's visibility or linkage, determines for a multiple source-file program whether the identifier is known only in the current source file or in any source file with proper declarations.

* Storage class specifiers 
  * Auto 
  * Register 
  * Extern 
  * Static 
* Durations
  * Automatic storage durations
  * Static storage duration 

Keyword Auto: Used to declare variables of automatic storage duration
* Created when the block in which they are defined is entered 
* Exist while the block is active 
* Destroyed when the block is exited 

## Local Variables

* Are declared within a function body or block of code
* Have automatic storage duration by default 
* Automatic local variables
  * Automatically created each time the function is called 
  * Their values are local to the function 
* The value of local variable cna only be accessed by the function in whhich the variable is defined 
  * Its value cannot be accessed by any other function 
  * If an inital value is given to a variable inside a function, that initial value is assigned to the variable each time the function is called 
* The c compiler assumes by default that any variable defined inside a function is an automatic local variable 
    * You can, however, make your intentions perfectly clear by explicitly using he keyword auto before the definition of the variable

## Why use Auto?

* Automatic storage is a means of conserving memory 
  * Exist only when they are needed 
  * They are created when the funciton in which they are defined is entered 
  * They are detroyed when the function is exited 
* Least privilege 
  * Allowign access to data only when it is absolutely needed

## Syntax 

Storage classes precede the type of the variable

```c
// storage_class var_data_type var_name

auto double x,y;
```
