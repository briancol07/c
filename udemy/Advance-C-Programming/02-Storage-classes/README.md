# Storage Classes 

## Overview 

## Summary 


| Storage Class | Declaration location | Scope(visibility) | LifeTime(alive) |
|:-------------:|:--------------------:|:-----------------:|:---------------:|
Auto  | inside a function| within the function/block| until the function/block completes           |
Register | Inside a function/block| withing the function/block |  until the function/block completes |
Extern | Outside all functions| Entire file plus other files wher the variable is declared as extern |until the program terminates |
static (local) | Inside a function/block |within the function/block| until the program terminates  |
Static (global)| Outside all functions|Entire file in which it is declared | until the program terminates |
