#include<stdio.h>

// no register int x = 10; 
// only in local blocks 

int main(){
  // this is not valid 
  // register int x = 14
  // int *a = &x;

  // this yes 

  int x = 14;
  register  int *a = &x;

  // no register static int *a;

  printf("\n %d",*a);
  return 1;
}
