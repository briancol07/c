# External 

## External variables 

The extern storage class simply tell us that a variable is defines elsewhere, not within the same block where it is used.

An extern variable is a global variable initialized with a legal valuewher it is declared in order to be used elswhere, as an extension to the concept of the global variable

The main purpose of using exter variables is that they can be accessed between two different files which are part of a large program.
Functions contained in separate file can communicate through external variables

The extern storage class is udes to give a reference of a global variable that is visible to all the program files 

Inside the module that wants to access the external variable: the variable data type is preceded with the keyword extern in the declaration, tells the compiler that a blobally defined variable from antoher file is to be accessed 

Suppose you want to define an int variable called moveNumber 

* You want to access the value and possibly modify it within a function contained in another file 

```c
int moveNumber = 0;
```

Declare the above at the beginning of your program, outside of any function, its value could be referenced by any function within that program, moveNumber is defined as a global variable.

This same definition of the variable moveNumber also makes its value accessible by functions contained in other files.

The statement defines the variable mvoeNumber not just as a global variable, but also as an external global variable

To refernece the value of an external global variable from another module 

* You must declare the variable to be accessed, preceding the declaration with the keyword extern 
* The variabel cannot be initialized 
  * It points the variable name at a storage location thathas been previously defined 

```c
extern int moveNumber;
```

The value of moveNumber can now be accessed and modified by the module in which the preceding declaration appears 
Other modules can also accessthe value of moveNumber by incorporating a similar extern declaration in the file 

## Rules

You must obey an important rule when workign with external variables 

* The variable hast to be defined in some place amogn your source files
* The first way is to declare the variable outside of any function, not preceded by the keyword exter n
* The second way to define an external variable is to declare the variable outside of any function, placing the keyword extern in front of the declaration 
  * Explicitly assigning an initial value to it 
* These two ways are mutually exclusive 

## Specifier on functions

When a function is defined,it can be declared to be extern explicitly 
An extern function can be called from a file where it is not defined 
Where it does not need tobe defined in a header file 

```c
extern double foo(double x){}
```

The definition of the foo function effectively becomes global to any file in the program
  * Can be called from outside the file 
