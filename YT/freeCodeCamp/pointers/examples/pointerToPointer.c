#include<stdio.h>

int main(){
  int x = 5;
  // Store address of x in p
  int *p = &x;
  // change value of x 
  *p = 6;
  // to store the address of p 
  int **q = &p;  
  // to store address of q 
  int ***r = &q;
  printf("%d \n",*p);
  printf("%d \n",*q); // The value in p (address)
  printf("%d \n",*(*q)); // to see the value where is pointing 
  printf("%d \n",*(*r)));

  printf("%d \n",*(*(*r)));
  ***r = 10;
  printf("x = %d \n",x);
  **q = *p +2;
  printf("x = %d \n",x);
}
