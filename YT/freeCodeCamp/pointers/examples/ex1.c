
#include<stdio.h>

int main(){
  int a = 10;
  int *p;
  p = &a;   // &a = address of a 
  printf("%d \n",p);
  printf("%d \n",*p); // p - value at adress point by p 
  printf("%d \n",&a); // address of a 
  printf("-------------\n");
  printf(" a = %d \n",a);
  *p = 12; // dereferencing  value at address p 
  printf(" a = %d \n",a);
}

