# Working with pointers 

``` c

int a;      // Integer
int *p;     // Pointer to integer 

char c;     // Character 
char *p0;   // Pointer to character 

double d;   // Double 
double *p1; // Pointer to double

```

This will no change the address of p 


```c 

int a;
int *p = &a;

*p = b;
```

we must do something like this to change it. 

```c 
p = &b
```

## Pointer arithmetic 

```c
printf("%d \n" ,p);// example p is 2002
printf("%d \n",p+1); // p+1 will be 2006 
// size of int 4 bytes 
```





