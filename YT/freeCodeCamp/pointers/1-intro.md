# Pointers 

## How data are store in memory 

ram --> segments and partitions 

have an address 

declare a variable in our program alocate an amount of memory  in the memory

int ->  4 bytes
char->  1 byte
float-> 4 bytes

in a look up table where store the var with the address 

> Pointers are variables that store address of another variable

```c 

int a;
int *p;

p = &a;

```

if we try to print p it will give the address 

print | result
------|--------
 p    | address a 
 &a   | address a 
 &p   | address p 
 \*   | number store in a 

The asterix in the pointer it will be called dereferencing (value at address p)

