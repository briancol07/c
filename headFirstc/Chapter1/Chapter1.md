# Chapter 1 


## The way C works 

Computers really only understand one language: machine code, a binary stream of 1s and 0s. You convert your C code into machine code with the aid of a compiler 


Source ---> Compile ---> Output 

Source|Compile|Output 
-----|--------|------
You start off by creating a source file. The source file contains human readable C code | You run your source code through a compiler. The compiler checks for errors, and once it's happy, it compiles the source code.| The compiler creates a new file called an executable. this file contains machine code, a stream of 1s and 0s that the computer understands . And that's the program you can run. 

> The Current Standard is c11

``` c 
// simple code 

int main(){
  printf("C Rocks");
  return 0;
}
```

### To compile the file (in linux)

> gcc filename.c -o name

This will create the executable file 

> ./name to execute it


