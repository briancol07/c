# Head first C 

- [ ] Getting Started with C: Diving in
- [ ] Memory and POinters: What are you pointing at?
- [ ] Creating Small Tools: Do one thing and do it well 
- [ ] Using Multiple source Files: Break it down, build it up 
- [ ] Structs, Unions and Bitfields: Rolling your own structures 
- [ ] Data Structures and Dynamic Memory : Building bridges 
- [ ] Advanced Functions
- [ ] Static and Dynamic Libraries 
- [ ] Processes and System Calls: Breaking boundaries
- [ ] Interprocess Communication
- [ ] Sockets and Networking 
- [ ] Threads 
