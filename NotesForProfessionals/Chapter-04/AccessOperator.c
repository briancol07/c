struct MyStruct{
  int x;
  int y;
};

int main(){
struct MyStruct myObject;
struct MyStruct *p = &myObject;
myObject.x = 42;
myObject.y = 123;

/* outputs 42 123 */
printf(".x = %i, .y = %i \n", myObject.x, myObject.y); 

/* Member of pointed-to Object */

printf(".x = %i, .y = %i \n", p->x, p->y); 

/* Address-of*/
/* Output "A=A" , for some implementation*/
int z = 3;
int *i = &z;
printf("%p = %p \n",(void *)&z, (void *)i);

/* Dereference */

int a = 42;
int *b = &a;
printf("a = %d, *b = %d\n", a, *b); /* Outputs "x = 42, *p = 42". */
*b = 123;
printf("a = %d, *b = %d\n", a, *b); /* Outputs "x = 123, *p = 123". */

/* Indexing */
int arr[] = { 1, 2, 3, 4, 5 };
printf("arr[2] = %i\n", arr[2]); /* Outputs "arr[2] = 3". */


/* Interchangeability of indexing */

printf("3[arr] = %i\n", 3[arr]); /* Outputs "3[arr] = 4". */
} 
