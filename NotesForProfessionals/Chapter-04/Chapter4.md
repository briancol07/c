# Chapter 4 

## Operators 

An operator in a programming language is a symbol that tells the compiler or interpreter to perform a specific mathematical, relational or logical operation and produce a final result 

* Binary 
* Unary 
* Ternary 

## Relational Operators 

Check if a specific relation between two operands is true , Its evaluate to 0(false) or 1 (true)
<br>

Meaning   | Symbol 
:--------:|:-------:
 Equals   | == 
 Not Equals | != 
 Not | ! 
 Greater than | > 
 Less than | < 
 Greater than or equal | >=
 Less than or equal | <=

## Conditional Operator / Ternary Operator 

Evaluates its first operand and if the resulting value is not equal to zero evaluates its second operand. Otherwise, it evaluates its third 


```c
a = b ? c : d ;

// Equivalent to 

if(b) 
  a = c ;
else 
  a = d; 
```

## Bitwise Operators 

can be used to perfom bit level operation on variables 
see [bitwise](./bitwiseOperator.c)

Symbol | Operator 
:-----:|:--------:
 & | bitwise AND 
\| | bitwise Inclusive OR
 ^ | bitwise Exclusive OR (XOR)
 \- | bitwise not (one's complement)
 << | Logical left shift 
 >> | Logical right shift 

Bitwise operations with signed types should be avoided because the sign bit of such a bit representation has particular meaning 

* Left shifting a 1 bit into the signed bit it erroneus and leads to undefined behavior
* Right shifting a negative value (with sign bit 1) is implementation defined and theresfore not portable
* If the value of the right operand of a shift operator is negative or is greater than or equal to the width of the promoted left operand, the behavior is undefined

### Masking 

> Masking refers to the process of extracting the desired bit from a variable by using logical bitwise operations. The operan(a constant or variable) that is used to perform masking is called a mask 

* To decide the bit pattern of an integer variable
* To copy a portion of a given bit pattern to a new variable, while the remainder of the new variable is filled with 0s using bitwise AND
* To copy a portion of a given bit pattern to anew variable, while the remainder of the new variables is filled with 1s(using bitwise OR) 
* To copy a portion of agiven bit pattern to a new variable while the remainder of the original bit pattern is inverted within the new variable (using exclusive OR) 

## Comma Operator 

> Evaluates its lef operand, discards the resulting value and then evaluates its rights operand and result yuekds the value of its rightmost operand 

```c
int x = 42 , y = 42;
printf("%i\n",(x*=2,y));  /* Output = 42*/
```
Introduces a sequence point between its operands 

## Arithmetic operators 

### Basic Arithmetic 

Return a value that is the result of pplying the left hand operand to the right hand operand using the mathematical operation. Normal mathematical rules of communication apply.

type | Symbol 
-----|-------
Addition | + 
Subtraction| -
Multiplication | *  
Division | /
Increment | ++
Decrement | -- 

## Access Operators 

see [Accesors](./AccessOperator.c) 

### Member of Object
The member acces operators (dot and arrow) are used to access a memver of a struct. 

### Member of pointed-to object 

> Syntactic sugar for dereferencing follow by memeber access. x->y , shorthand for (\*x).y but the arrow operator is much clearer 

### Address-of
The unary & operator is address of operator. it evaluates the given expression where the resulting object must be an ivalue. Then, it evaluates into an object whose type is a pointer to the resulting object's type and contains the address of the resulting object object

### Dereference 

The unary * operator dereferences a pointer. It evaluates into the Ivalue resulting from dereferencing that result from evaluating the given expression

### Indexing 

Indexing is syntactic sugar for pointer addition followed by dereferencing. Effectively an expression of the form a[i] is equivalent to \*(a+i) but the explicit subscripts notation is preferred

### Interchangeability of index 

Adding a pointer to an integer is a commutative operation (i.e. the order of the operands does not change the
result) so pointer + integer == integer + pointer .
A consequence of this is that arr[3] and 3[arr] are equivalent.

## Size of Operator 

Evaluates into the size in bytes, of type size_t of object the given type requires parentheses around 

```c 
printf("%zu\n", sizeof(int)); 
/* Valid, outputs the size of an int object, which is platform-dependent. */
printf("%zu\n", sizeof int); 
/* Invalid, types as arguments need to be surrounded by parentheses! */
```
## Cast Operator 

Perfoms an explicit conversion into the given type from the value result from evaluating the given expression

```c
int x = 3;
int y = 4;
printf("%f\n", (double)x / y); /* Outputs "0.750000". */
```
## Function call operator 

[FuntionCall](./FunctionCall.c) 

you can call the function by doing this: FunctionName(arguments)

## Increment/Decrement 

``` c
int a = 1;
int b = 1;
int tmp = 0;

tmp = ++a;
/*increments a by one and return new value; a ==2, temp == 2 */
tmp = a++;
/*increments a by one and return new value; a ==3, temp == 2 */
tmp = --b;
/*increments a by one and return new value; b ==0, temp == 0 */
tmp = b--;
/*increments a by one and return new value; a ==-1, temp == 0 */
```

## Assignment Operator 

```c 

int x = 5;
char y = 'c';
/* Variable x holds the value 5. Returns 5. */
/* Variable y holds the value 99. Returns 99
* (as the character 'c' is represented in the ASCII table with 99).
*/
float z = 1.5; /* variable z holds the value 1.5. Returns 1.5. */
char const* s = "foo"; /* Variable s holds the address of the first character of the string 'foo'.
*/
```
### Several arithmetical operation have a compound addignment operator

```c
a+=b    /*equal to: a = a + b */ 
a-=b    /*equal to: a = a - b */
a*=b    /*equal to: a = a * b */
a/=b    /*equal to: a = a / b */
a%=b    /*equal to: a = a % b */
a&=b    /*equal to: a = a & b */
a|=b    /*equal to: a = a | b */
a^=b    /*equal to: a = a ^ b */
a<<=b   /*equal to: a = a << b */
a>>=b   /*equal to: a = a >> b */
```
### Operator Associativity 

```c 
int a,b=1,c=2;
a = b = c;
``` 

## Logical Operators

### And 

```c 
0 && 0 /* Return 0 */
0 && 1 /* Return 0 */
2 && 0 /* Return 0 */
2 && 3 /* Return 1 */
``` 
### Or

```c 

0 || 0 /* Returns 0 */
0 || 1 /* Returns 1 */
2 || 0 /* Returns 1 */
2 || 3 /* Returns 1 */
```

### Logical NOT

```c 
!1 /* Returns 0*/
!7 /* Returns 0*/
!0 /* Returns 1*/
```
There are some crucial properties common to both && and || :
the left-hand operand (LHS) is fully evaluated before the right-hand operand (RHS) is evaluated at all,
there is a sequence point between the evaluation of the left-hand operand and the right-hand operand,
and, most importantly, the right-hand operand is not evaluated at all if the result of the left-hand operand
determines the overall result.
This means that:
if the LHS evaluates to 'true' (non-zero), the RHS of || will not be evaluated (because the result of 'true OR
  anything' is 'true'),
  if the LHS evaluates to 'false' (zero), the RHS of && will not be evaluated (because the result of 'false AND
    anything' is 'false').

## Pointer Arithmetic 

```c
int arr[] = {1, 2, 3, 4, 5};
printf("*(arr + 3) = %i\n", *(arr + 3)); /* Outputs "4", arr's fourth element. */
```
