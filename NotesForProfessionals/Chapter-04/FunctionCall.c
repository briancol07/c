int myFunction(int x, int y){
  return x * 2 + y;
}

int main(){
  int (*fn)(int,int) = &myFunction;
  int x = 42;
  int y = 123;
  printf("(*fn)(%i, %i) = %i\n", x, y, (*fn)(x, y));
  /* Outputs "fn(42, 123) = 207". */
  printf("fn(%i, %i) = %i\n", x, y, fn(x, y));
  /* Another form: you don't need to dereference explicity*/

}
