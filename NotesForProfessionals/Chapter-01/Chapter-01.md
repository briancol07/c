# Chapter 1 

## Hello world 

It's a program in text file called hello.c 

```c
#include <stdio.h>
```
This line tell the compiler to include the contents of the standr library header (stdio.h)

* Contains 
  * Function declarations 
  * Macros 
  * Data types 

In this case includes the function **puts()** 

```c
int main(void)
```

This line starts the definition of a function.The type of arguments it expects (void ,meaning none) and the type of value that return(int, integer).
The curly braces are used in pairs to indicate where a block of code begins and ends 

```c 
puts("hello,world");
```

This calls the puts() function to output text to standard output (screen by default), string must be in **" "** 

```c
return 0;
```

Every statement need to be terminated by a semi-colon(;)

When we defined main (), we declared it as function returning int meaning it need to return an integer, in this case 0 indicate that the rpgram exited successfully ,after this will end 


## Compiling and running the program 

1. Compiling using gcc 
  * gcc hello.c -o hello
2. Warning options 
  * gcc -Wall -Wextra -Werror -o hello hello.c
3. Using the clang compiler 
  * clang -Wall -Wextra -Werror -o hello hello.c 

## Executing the file 

```bash 
./hello
``` 

