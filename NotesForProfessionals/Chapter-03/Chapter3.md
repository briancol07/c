# Chapter 3 

## Data Types 

* Unary "dereference" 
  * * 
  * Pointer 
* Binary "array subscription"
  * {}
  * Array
* (1+n)-ary "function call"
  * ()
  * Function

## Example 

char \*name[20];

In this case take precedence [] over * , so the interpretation is : names is an array of size 20 of a pointer to char 

char (\*place)[10];

In case of using parentheses to override the precedence , The * is applied first : is a pointer to an array of size 10 of char

int fn(long,shor);

fn is a function taking long,short and returning int

int \*fn(void);

The () is applied first so fn is a function taking void and returning a pointer to int 

int \*\*ptr; 

The two dereference operators have equal precedence, so the associativity take effect. The operators are applied in right to left so its a pointer to a pointer to an int 

## Multiple declarations 

```c 
int fn(void), *ptr , (*fp)(int), arr[10][20],num;
```
## Fixed width integer

```c
uint32_t u32 = 32 //exactly 32-bit wide   
uint8_t u8 = 255 // exactly 8-bit wide
int64_t i64 = -65 // exactly 64 bit in two's complement representation
```
## Integer types and constants 

```c 

signed char c = 127 // 1 byte 
signed short int si = 32767 // 16 bits 
signed int i = 32767 // 16 bits 
signed long int li = 2147483647 // 32 bits 
signed long long int li = 2147483647 // 64 bits (version > c99

unsigned int i 
unsigned short  
unsigned char 
```
## Different type

``` c

int decimal = 42 ;
int octal = 052;
int x = 0xaf;
int X = 0XAF;

``` 
## suffixes to describre width and signedness

```c 
long int i = 0x32; // no suffix represent int or long int 
unsigned int ui = 65535u; // u or U represent unsigned int/long int
long int li = 655361l; // l or L represent long int 
```

## FloatingPoint Constants 

```c 
float f = 0.314f;        // f ==> float 
double d = 0.314;        // no suffix denotes double
long double ld = 0.314l; // l/L ==> long double
double x = 1.;           // valid fractinal part is optional
double y = .1;           // valid whole-numbe part is optinal 
double sd = 1.2e3;       // decimal is scaled by 10^3 
```
you can use the header \<float.h>

## String literals 

```c 
char * str = "hello,world";

char a1[] = "abc" // a1 is char[4] {'a','b','c','\0'}
char a2[4] = "abc" // same as a1
char a3[3] = "abc" // missing the '\0'
```

String literals are not modifiable (.rodata)

```c 
/* normal string literal, of type char[] */
char* s1 ="abc";
/* wide character string literal, of type wchar_t[] */
wchar_t* s2 = L"abc"; 
/* UTF-8 string literal, of type char[] */
char* s3 = u8"abc";
/* 16-bit wide string literal, of type char16_t[] */
char16_t* s4 = u"abc";
/* 32-bit wide string literal, of type char32_t[] */
char32_t* s5 = U"abc";
```

