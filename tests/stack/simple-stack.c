#include<stdio.h>
#include<stdlib.h>

struct stackNode{
  
  int data; // data as int 
            // Depend the quantity and types can be more than one 
  struct stackNode *nextPtr; // Stack node pointer 
};

typedef struct stackNode StackNode; //synonym ofr struct stackNode 
typedef StackNode *StackNodePtr; // Synonym for StackNode* 

// Prototypes 

void push(StackNodePtr *topPtr, int info); // parameters depend 
int pop(StackNodePtr *topPtr);
int isEmpty(StackNodePtr topPtr);
void printStack(StackNodePtr currentPtr);
void instructions(void);


int main(){
  StackNodePtr stackPtr = NULL;  // point to stack top 
  int value = 0; 

  instructions();
  int choice = 0 ; // default value 
  scanf("%d",&choice);

  while(choice != 3){
    switch(choice){
      case 1: // push 
        printf("Enter an integer \n");
        scanf("%d",&value);
        push(&stackPtr,value);
        printStack(stackPtr);
        break;
      case 2:
        if(!isEmpty(stackPtr)){
          printf("The popped value is %d.\n",pop(&stackPtr));
        }
        printStack(stackPtr);
        break;
      default:
        printf("invalid choice \n");
        instructions();
        break;
        }
    instructions();
    scanf("%d",&choice);

    }
  printf("The end\n");
}

void instructions(void){
  printf("Enter choice,\n");
  printf("1 to push \n");
  printf("2 to pop \n");
  printf("3 to end \n");
}

void push(StackNodePtr *topPtr, int info){
  StackNodePtr newPtr = malloc(sizeof(StackNode)); // memory allocation  create a new stack 
  if (newPtr != NULL){
    newPtr->data = info;  // this will load info to data 
                          // all the data types should be modify here to new ones 
    newPtr->nextPtr = *topPtr; // this change the address of the top pointer, to point another one
    *topPtr = newPtr; 
  }
  else {
    printf("%d not inserted, no memory \n", info);
  }
}

// STACK = LIFO  last input first output 
int pop(StackNodePtr *topPtr){
  StackNodePtr tempPtr = *topPtr;
  int popValue = (*topPtr)->data;
  *topPtr = (*topPtr)->nextPtr; // get the data from the top pointer 
  free(tempPtr); // free memory of tempPtr 
  return popValue;
}

void printStack(StackNodePtr currentPtr){
  if (currentPtr == NULL){  // check if its empty 
    printf("Stack empty");
  }
  else {
    printf("The stack is");
    while (currentPtr != NULL){ // while not the end of stack 
      printf("%d --> ",currentPtr->data);
      currentPtr = currentPtr -> nextPtr; // points to the next one 

    }
    printf("NULL\n");
  }
}

int isEmpty(StackNodePtr topPtr){
  return topPtr == NULL;
}



