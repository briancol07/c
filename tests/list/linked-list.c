#include<stdio.h>
#include<stdlib.h>

struct listNode{
  char data;
  struct listNode *nextPtr;
};

typedef struct listNode ListNode;
typedef ListNode *ListNodePtr;

// prototypes 

void insert(ListNodePtr *sPtr, char value);
char delete(ListNodePtr *sPtr, char value);
int isEmpty(ListNodePtr currentPtr);
void printList(ListNodePtr currentPtr);
void instructions(void);

int main(void){
  ListNodePtr startPtr = NULL;
  char item = '\0';
  instructions();
  printf("%s","? ");
  int choice = 0;

  scanf("%d",&choice);

  while(choice !=3){
    switch (choice){
      case 1: // insert 
        printf("%s","Enter character: ");
        scanf("\n%c",&item);
        insert(&startPtr, item);
        printList(startPtr);
        break;
      case 2: // Delete 
        if (!isEmpty(startPtr)){
          printf("%s","Enter character to be deleted");
          scanf("\n%c",&item);
          if (delete(&startPtr, item)){
            printf("%c deleted \n", item);
            printList(startPtr);
          }else{
            printf("%c not found \n\n",item);
            }
          }else{
            puts("list is empty\n");
        }
        break;
      default:
        puts("invalid choice \n");
        instructions();
        break;
    }
    printf("%s","? ");
    scanf("%d",&choice);
  }

  puts("End of run");
  return 0;

}

void instructions(void){
  puts("Enter choice \n"
    " 1 to insert an element into list \n"
    " 2 to delete an element from the list\n"
    " 3 to End");
}

void insert(ListNodePtr *sPtr, char value){
  ListNodePtr newPtr = malloc(sizeof(ListNode)); // create node 
  
  if(newPtr != NULL){
    newPtr->data = value;
    newPtr->nextPtr = NULL;

    ListNodePtr previousPtr = NULL;
    ListNodePtr currentPtr = *sPtr;

    // LOOP to find correct location in the list 
    while(currentPtr != NULL && value > currentPtr->data){
      previousPtr = currentPtr;
      currentPtr = currentPtr->nextPtr;
    }
    // insert new node at beginnign of list 
    if (previousPtr == NULL){
      newPtr->nextPtr = *sPtr;
      *sPtr = newPtr;
    }else {
      // Insert new node between previousPtr and currentPtr 
      previousPtr->nextPtr = newPtr;
      newPtr->nextPtr = currentPtr;
      }
    }else{
      printf("%c not inserted. No memory available. \n",value);
    }
  }
// receives the address of the pointer to the list's first node and a character to delete
char delete(ListNodePtr *sPtr, char value){
  if (value == (*sPtr)->data){
    ListNodePtr tempPtr = *sPtr;
    *sPtr = (*sPtr)->nextPtr;
    free(tempPtr);
    return value;
  }
  else{
    ListNodePtr previousPtr = *sPtr;
    ListNodePtr currentPtr = (*sPtr)->nextPtr;

    // loop to find the correct location in the list
    while(currentPtr != NULL && currentPtr->data != value){
      previousPtr = currentPtr;
      currentPtr = currentPtr->nextPtr;
    }

    // delete node at currentPtr
    if(currentPtr != NULL){
      ListNodePtr tempPtr = currentPtr;
      previousPtr->nextPtr = currentPtr->nextPtr;
      free(tempPtr);
      return value;
    }
  }
  return '\0';
}
// 1 = empty 0 = no 
int isEmpty(ListNodePtr sPtr){
  return sPtr == NULL;
}

void  printList(ListNodePtr currentPtr){
  if(isEmpty(currentPtr)){
    puts("List is empty \n");
  }
  else{
    puts("The list is:");
    while(currentPtr != NULL){
      printf("%c --> ",currentPtr->data);
      currentPtr = currentPtr->nextPtr;
    }
    puts("NULL\n");
  }
}
