#include<stdio.h>

int main(){

  // Ejercicio 1 

  int num ; 

  printf("Ingreseme un numero\n");

// Pedir numero al usuario
  scanf("%d",&num);
  

  if (num > 0) printf("El numero ingresado es positivo\n");

  // Ejercicio 2 

  if(num == 0) printf("El numero ingresado es cero\n");

//---------------------------------------------------------------
  // If - Else
//---------------------------------------------------------------


// Ejercicio 6

  if (num > 0) printf("El numero ingresado es positivo\n");
  else printf("El numero ingresado es Negativo\n");

// Ejercicio 7 

  printf("Ejercio 7---- \n");

  if (num > 0) printf(" El numero es positivo\n");
  else if (num < 0) printf( "El numero es negativo");
  else printf("El numero es cero\n");

//---------------------------------------------------------------
  // Switch 
//---------------------------------------------------------------

// "comillas doble cadena de caracteres "
// 'a' Comilla simple para caracteres 

char var = 'a';
char var2 = 'b';

switch(var):
  case 'a':
    printf("la var es = a ");
    break;
  case var2:
    printf("la var es = b ");
    break;
  default :
    printf("la variable no es ni a ni b ");
 

  return 0 ;
}
