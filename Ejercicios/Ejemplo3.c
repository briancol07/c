#include<stdio.h>

int main(){

  //----------------------------------------------
  // While -> hacer mientras la condicion sea Verdadera
  //----------------------------------------------

  // Ejercicio 1 

  int num = 1;

// < > >= <= == != 
// Va del 1 al 1000

  while(num <= 1000){
    printf("El numero es : %d\n",num);
    num ++ ; // post incremento == num = num + 1
  };

  // Ejercicio 2 

  int contrasenia= 1234;

  int aux = 0;

  while(aux != contrasenia){
    printf("Ingrese contrasenia\n");
    scanf("%d",&aux);
  }


  //----------------------------------------------
  // Do - While ---> hacer mientras sea verdadero
  //----------------------------------------------

  // Diferencia con el while es que almenos se hace 1 vez 

  // Ejercicio 7  

  int num_1 ;
  int num_suma = 0; 

  do {
    printf("Ingrese un numero \n");
    scanf("%d",&num_1);

    num_suma = num_suma + num_1 ;
    printf("La suma es %d\n",num_suma);
  }while(num_1 > 0);

  return 0 ; 
}


